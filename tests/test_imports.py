import pytest
import importlib


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@pytest.mark.parametrize(
    "import_loc",
    (
        'ctg.aact.build',
        'ctg.aact.parsers',
        'ctg.aact.type_casts',
        'ctg.aact.constants',
        'ctg.aact.orm',
        'ctg.aact.parse_schema',
        'ctg.example_data.examples',
    )
)
def test_package_import(import_loc):
    """Test that the modules can be imported.
    """
    importlib.import_module(import_loc, package=None)
