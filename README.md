# Clinical Trials

__version__: `0.1.0a1`

The clinical-trials package is a toolkit to to build the [clinicaltrials.gov](https://clinicaltrials.gov/) database that is distributed by [AACT](https://aact.ctti-clinicaltrials.org/).

This project is distinct from the clinical trials initiative and AACT. There is [online](https://cfinan.gitlab.io/clinical-trials/index.html) documentation for clinical-trials.

## Installation instructions
At present, clinical-trials is undergoing development and no packages exist yet on PyPi. Therefore it is recommended that you install via conda unless you have access to gwas-norm, in which case you can use a pip install from the git repository.

### Installation using conda
I maintain a conda package in my personal conda channel. To install from this please run:

```
conda install -c cfin -c bioconda -c conda-forge clinical-trials
```

There are currently builds for Python v3.8, v3.9, v3.10 for Linux-64 and Mac-osX.

Please keep in mind that all development is carried out on Linux-64 and Python v3.8/v3.9. I do not own a Mac so can't test on one, the conda build does run some import tests but that is it.

### Installation using pip
You can install using pip from the root of the cloned repository, first clone and cd into the repository root:

```
git clone git@gitlab.com:cfinan/clinical-trials.git
cd clinical-trials
```

Install the dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

Then install using pip
```
python -m pip install .
```

Or for an editable (developer) install run the command below from the root of the repository. The difference with this is that you can just to a `git pull` to update, or switch branches without re-installing:
```
python -m pip install -e .
```

### Conda dependencies
There are also conda yaml environment files in `./resources/conda/envs` that have the same contents as `requirements.txt` but for conda packages, so all the pre-requisites. I use this to install all the requirements via conda and then install the package as an editable pip install.

However, if you find these useful then please use them. There are Conda environments for Python v3.8, v3.9, v3.10.

## Database configuration
The package uses SQLAlchemy to handle database interaction. This means that in theory you are not restricted to a particular database backend. In practice most testing/development will be performed against SQLite and MySQL. So, if you use something else and run into issues, please submit an issue or get in contact.

Any database connection parameters can be supplied on the command line or via a configuration file. You can supply a full connection URL directly on the command line. However, this is not a good idea if your database is password protected. In this case you should supply the connection parameters in a connection file `.ini` file. They should be set out as below:

```
[my_db_sqlite_conn]
# An SQLAlchemy connection URL, see:
# https://docs.sqlalchemy.org/en/13/core/engines.html#sqlalchemy.engine_from_config
# All connection options here should be prefixed with umls
# Make sure passwords are URL escaped:
# import urllib.parse
# PW = urllib.parse.quote_plus(PW)
# Also, don't forget to escape any % that are in the URL (with a second %)
db.url = sqlite:////data/my_db.db

[my_db_mysql_conn]
db.url = mysql+pymysql://user:password@hostname/my_db

[my_db_postgres_conn]
db.url = postgresql+psycopg2://://user:password@hostname/my_db
```

Then to use these from the command line you can supply the section header from the config file to the script, so ``my_db_mysql_conn`` for the connection to the MySQL database.
