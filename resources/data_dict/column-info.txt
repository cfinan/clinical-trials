column	name	data_type	description
1	table_name	string	The current name of the database table, must not contain any spaces. If the table name has been renamed in the table-info file, then this table name must be the new name.
2	column_name	string	The name of the column.
3	new_column_name	string	Placeholder for any re-naming of the column - should be empty.
4	dtype	string	The column data dtype, most will be empty but can be updated here. Currently supported data types are. ``int``, ``numeric``, ``big_int``, ``small_int``, ``text``, ``varchar``, ``date``, ``datetime``, float``.
5	max_len	integer	Placeholder for the maximum string length in a column. Will be empty.
6	is_primary_key	boolean	Is the column a primary key column (0/1) for (no/yes).
7	is_index	boolean	Is the column indexed (0/1) for (no/yes).
8	is_nullable	boolean	Can the column values be undefined (0/1) for (no/yes).
9	is_unique	boolean	Should the column values be unique (0/1) for (no/yes).
10	foreign_key	string	A foreign key definition for the column. This must be given as ``<table-name>.<column-name>``. If the table name has been changed in the table info file then the new table name must be used here.
11	column_doc	string	A description for the column.
