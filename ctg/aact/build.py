"""Command line tool for building
`clinicaltrials.gov <https://clinicaltrials.gov>`_ from
`AACT <https://aact.ctti-clinicaltrials.org/download>`_. This uses
`SQLAlchemy <https://docs.sqlalchemy.org/en/14/>`_ to parse the pipe delimited
files into a relational database. These files are distributed in a zip archive.
This build script will extract directly from the zip archive and build. The zip
archive name/structure should not be altered.

Note that the zip downloads file names from AACT are as hash strings and not
human readable. This is unfortunate otherwise I would have used the file name
to encode some version info into the database.
"""
# Importing the version number (in __init__.py) and the package name
from ctg import (
    __version__,
    __name__ as pkg_name,
)
from ctg.aact import (
    orm as o,
    constants as con
)
from pyaddons import log, utils
from sqlalchemy_config import (
    config as cfg,
    utils as ocom
)
from sqlalchemy import (
    String,
    Text,
    BigInteger,
    SmallInteger,
    Integer,
    Numeric,
    Float,
    Date,
    DateTime,
    Boolean
)
# from sqlalchemy.sql.sqltypes import (
#     String
# )
from zipfile import ZipFile
from datetime import datetime
from collections import namedtuple
from tqdm import tqdm
import shutil
import glob
import argparse
import os
import re
import sys
import csv
import tempfile
# import pprint as pp


_PROG_NAME = 'ctg-aact-build'
"""The name of the program (`str`)
"""
_DESC = __doc__
"""The program description (`str`)
"""


_DEFAULT_PREFIX = 'db.'
"""The default SQLAlchemy connection prefix in the config file.
"""
FileInfo = namedtuple('FileInfo', ['path', 'type', 'version', 'date'])
"""Will hold information on the ZIP archive and the XML files extracted from
the zip file (`FileInfo`)
"""

# Some parsing constants
BOOLS = [
    (re.compile('^t(rue)?$', re.IGNORECASE), 1),
    (re.compile('^f(alse)?$', re.IGNORECASE), 0),
    (re.compile('^0$', re.IGNORECASE), 1),
    (re.compile('^1$', re.IGNORECASE), 0),
    (re.compile('^y(es)?$', re.IGNORECASE), 1),
    (re.compile('^n(o)?$', re.IGNORECASE), 0),
]
"""Regular expressions that indicate string boolean values, these will
only be applied if the number of values observed is 2
(`list` of `re.Pattern`)
"""
STRING_DATES = [
    '%Y-%m-%d', '%B %Y', '%B %-d,%Y'
]
"""Date parsing templates that indicate if string values are actually dates
(`list` of `str`)
"""
STRING_DATE_TIME = [
    '%Y-%m-%d %H:%M:%S.%f'
]
"""Date parsing templates that indicate if string values are actually
datetimes (`list` of `str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API usage see
    ``ctg_aact.build.build_clinical_trials``
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(_PROG_NAME, verbose=args.verbose)
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    # Get a sessionmaker to create sessions to interact with the
    # database
    sm = cfg.get_new_sessionmaker(
        args.dburl, conn_prefix=_DEFAULT_PREFIX,
        config_arg=args.config, config_env=None,
        config_default=cfg.DEFAULT_CONFIG, exists=False
    )

    try:
        # Make sure the database exists (or does not exist)
        cfg.create_db(sm, exists=False)
    except FileExistsError:
        # Check for zero length tables
        pass

    # Create a session
    session = sm()

    try:
        # Call the high level API function to build the database
        build_clinical_trials(
            session, args.inzip, verbose=args.verbose, tmpdir=args.tmpdir
        )
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        log.log_interrupt(logger)
    finally:
        session.close()
        log.log_end(logger)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )

    parser.add_argument(
        'inzip',
        type=str,
        help="The input zip archive downloaded from  "
        "https://aact.ctti-clinicaltrials.org/pipe_files"
        ", do not unzip"
    )
    parser.add_argument(
        'dburl',
        nargs='?',
        type=str,
        help="An SQLAlchemy connection URL, config section"
        " heading or filename if using SQLite."
    )
    parser.add_argument(
        '-c', '--config',
        type=str,
        default="~/.db.cnf",
        help="The location of the config file"
    )
    parser.add_argument(
        '-T', '--tmpdir',
        type=str,
        help="The location of tmp, if not provided will "
        "use the system tmp"
    )
    parser.add_argument(
        '-v', '--verbose',  action="count",
        help="give more output, use -vv to turn on progress monitoring"
    )
    parser.add_argument(
        '-e', '--exists',  action="store_true",
        help="The database already exists, this will assume"
        " that the user has pre-created a database and will "
        "not throw errors if the DB/tables already exist "
    )

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()
    args.config = utils.get_full_path(args.config)
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def build_clinical_trials(session, infile, verbose=False, tmpdir=None,
                          load_every=50000):
    """Main build function for the AACT clinicaltrials.gov implementation.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session to issue queries.
    infile : `str`
        The full path to the input AACT zip file containing the pipe delimited
        flat files, one per table and where the flat file basename is the
        table name.
    verbose : `bool` or `int`, optional, default: `False`
        Log progress, use 2 to turn on progress monitoring.
    tmpdir : `str`, optional, default: `NoneType`
        The tmp location, the zip file is decompressed to temp before being
        loaded. If not supplied then the system default temp location is
        used.
    load_every : `int`, optional, default: `50000`
        The number of rows to buffer up before committing to the database.
    """
    # Set up the logging according to the verbose level
    logger = log.retrieve_logger(_PROG_NAME, verbose=verbose)
    prog_verbose = log.progress_verbose(verbose=verbose)

    logger.info("creating tables...")

    # Make sure all the tables are present
    o.Base.metadata.create_all(
        session.get_bind()
    )

    # Get all the ORM Mappers in the ORM code
    orm_mappers = ocom.get_orm_mappers(o)

    # Based on the table structure set parsers for each column
    parsers = dict()
    for tn, t in orm_mappers.items():
        parsers[tn] = _set_parsers(t)

    # Now make sure the existing tables have 0 rows, i.e. we have no existing
    # data or partial load from a previous run
    _check_table_row_count(session, orm_mappers)

    # Create a temp dir to extract the zip into (will be deleted at the end)
    temp_dir = tempfile.mkdtemp(prefix="ctg_aact_", dir=tmpdir)
    try:
        logger.info(f"unzipping: {os.path.basename(infile)}")
        files = extract_data(infile, temp_dir)
        # files = glob.glob("{0}/*.txt".format(temp_dir))
        # Now get the table names from the file names
        files = _extract_file_tables(files)

        # Now we a good to load
        tqdm_kwargs = dict(
            desc="[info] loading tables", unit=" table",
            disable=not verbose, leave=True
        )

        # We need to load the studies table and a few others first, due to
        # FK relationships
        table_order = sorted(orm_mappers.keys(), key=con.SORT_TABLES)
        for tn in tqdm(table_order, **tqdm_kwargs):
            # Get the ORM mapping class
            t = orm_mappers[tn]

            # Load
            _load_table(session, t, files[tn], parsers, verbose=prog_verbose,
                        load_every=load_every)

    finally:
        shutil.rmtree(temp_dir)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _load_table(session, table, file_name, parsers, verbose=False,
                load_every=50000):
    """Load a single table into the database.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session to issue queries.
    table : `ctg_aact.orm.DeclarativeBase`
        An ORM Mapper class defined in the ``ctg_aact.orm`` module.
    file_name : `str`
        The full path to the input pipe delimited table file that matches the
        table mapper class.
    parsers : `dict` or `dict`
        All of the parsers. The keys are table names and the values are
        dicts of column names vs. parsing functions for each column.
    verbose : `bool`, optional, default: `False`
        Log progress monitoring.
    load_every : `int`, optional, default: `50000`
        The number of rows to buffer up before committing to the database.
    """
    # Get the table name
    tn = table.__table__.name
    tqdm_kwargs = dict(
        desc=f"[info] loading table: {tn}", unit=" rows",
        disable=not verbose, leave=False
    )

    try:
        # Get the parser for a table
        parser = parsers[tn]
    except KeyError as e:
        raise KeyError(f"can't find parser for table: {tn}") from e

    # Now open the input file
    open_method = utils.get_open_method(file_name)
    with open_method(file_name, 'rt') as infile:
        reader = csv.reader(infile, delimiter=con.AACT_DELIMITER)
        header = next(reader)

        try:
            # Now extract the parsing functions in the order we need them
            row_parse = [parser[i] for i in header]
        except KeyError as e:
            raise KeyError(f"problem with table: {tn}") from e

        if len(header) != len(parser):
            raise IndexError(f"unexpected header length in {tn}")

        row_buffer = []
        idx = 0
        for row in tqdm(reader, **tqdm_kwargs):
            idx += 1
            # Parse the row to make sure that missing values are represented as
            # NoneType
            row = [i if i not in con.MISSING_DATA else None for i in row]
            try:
                # Now apply the parsing functions to each column in the row
                # and convert the row into a dict with the column names as
                # keys
                row = dict(
                    [(h, p(i)) for i, p, h in zip(row, row_parse, header)]
                )
            except Exception:
                # We have errored out so get the specific column
                for i, p, h in zip(row, row_parse, header):
                    try:
                        p(i)
                    except Exception as e:
                        raise e.__class__(
                            f"{tn} problem with value {i} in column {h}"
                            f" row {idx}:"
                        ) from e

            # Add to the row buffer and if the buffer is full then load/commit
            row_buffer.append(row)
            if len(row_buffer) == load_every:
                session.bulk_insert_mappings(table, row_buffer)
                session.commit()
                row_buffer = []

        # Final load
        if len(row_buffer) > 0:
            session.bulk_insert_mappings(table, row_buffer)
            session.commit()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def extract_data(inzip, out_dir):
    """Extract all the files from the ZIP archive.

    Parameters
    ----------
    inzip : `str`
        The input ZIP archive
    out_dir : `str`
        The directory to extract the ZIP archive into

    Returns
    -------
    extracted_files : `list` or `str`
        the paths to the files extracted from the archive.
    """
    start_files = glob.glob(os.path.join(out_dir, "*"))
    with ZipFile(inzip, 'r') as zip:
        # extract all files to another directory
        zip.extractall(out_dir)
    end_files = glob.glob(os.path.join(out_dir, "*"))
    return [i for i in end_files if i not in start_files]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _extract_file_tables(files):
    """Extract all the table names from the file names. These are the file
    basenames with the .txt extension removed.

    Parameters
    ----------
    files : `list` of `str`
        The full paths to the input files.

    Returns
    -------
    table_files : `dict` of {`str`: `str`}
        The tables names are keys and the file paths are values.
    """
    table_files = dict()
    for i in files:
        table_files[re.sub('.txt$', '', os.path.basename(i))] = i
    return table_files


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _check_table_row_count(session, orm_tables):
    """Make sure that all the tables in the database are empty.

    Parameters
    ----------
    session : `sqlalchemy.Session`
        The SQLAlchemy session object to issue queries against the database.
    tables : `dict` of `{`str`: `sqlalchemy_config.orm_code.TableInfo`}`
        The table objects with the data types extracted. These contain an
        SQLAlchemy table object that can be used for queries.

    Raises
    ------
    ValueError
        If any table has > 0 rows.
    """
    for k, v in orm_tables.items():
        nrows = session.query(v).limit(10).count()

        if nrows > 0:
            raise ValueError(
                f"table contains existing rows: {k}({nrows})"
            )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _set_parsers(table):
    """Set all the parsers for the column in a table.

    Parameters
    ----------
    table : `sqlalchemy_config.orm_code.TableInfo`
        The table object that will provide the columns.

    Returns
    -------
    parsers : `dict` of {`str`: `func`}
        The keys are column names and the values are parsing functions.
    """
    return dict([(c.name, _set_basic_parser(c)) for c in table.__table__.c])


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _set_basic_parser(column):
    """Set the basic parser for a column.

    Parameters
    ----------
    column : `sqlalchemy_config.orm_code.TableColumn`
        The columns with an enumerated data type.

    Returns
    -------
    parse_func : `func`
        The parsing function to parse a single data point from that column.
    """
    if isinstance(column.type, (String, Text)):
        if column.nullable:
            return parse_str_none
        return parse_str
    elif isinstance(column.type, (Integer, BigInteger, SmallInteger)):
        if column.nullable:
            return parse_int_none
        return int
    elif isinstance(column.type, Boolean):
        if column.nullable:
            return parse_bool_none
        return parse_bool
    elif isinstance(column.type, (Float, Numeric)):
        if column.nullable:
            return parse_float_none
        return float
    elif isinstance(column.type, Date):
        if column.nullable:
            return parse_date_none
        return parse_date
    elif isinstance(column.type, DateTime):
        if column.nullable:
            return parse_date_time_none
        return parse_date_time
    else:
        raise TypeError(
            f"unknown data type for column: {column.name}({column.type})"
        )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_str(x):
    """Parse string data.

    Parameters
    ----------
    x : `str`
        The value to parse.

    Returns
    -------
    x : `str`
        The parsed string, this will have leading or lagging whitespace
        removed.
    """
    return x.strip()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_str_none(x):
    """Parse string or ``NoneType`` data.

    Parameters
    ----------
    x : `str` or `NoneType`
        The value to parse.

    Returns
    -------
    x : `str` or `NoneType`
        The parsed string or `NoneType`, a string will have leading or lagging
        whitespace removed.
    """
    if x is None:
        return x
    return x.strip()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_int_none(x):
    """Parse an integer or ``NoneType`` data.

    Parameters
    ----------
    x : `int` or `NoneType`
        The value to parse.

    Returns
    -------
    x : `int` or `NoneType`
        The parsed integer or `NoneType`.
    """
    if x is None:
        return x
    return int(x)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_float_none(x):
    """Parse `float` or ``NoneType`` data.

    Parameters
    ----------
    x : `float` or `NoneType`
        The value to parse.

    Returns
    -------
    x : `float` or `NoneType`
        The parsed `float` or `NoneType`.
    """
    if x is None:
        return x
    return float(x)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_bool(x):
    """Parse Boolean data, this may be 0/1 or several string variations such as
    T/F, Y/N (case insensitive) also longform (yes/no) etc...

    Parameters
    ----------
    x : `str`
        The value to parse.

    Returns
    -------
    x : `bool`
        The parsed Boolean.
    """
    x = x.strip()
    for reg, i in BOOLS:
        if reg.match(x):
            return i
    raise TypeError(f"unknown boolean: {x}")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_bool_none(x):
    """Parse Boolean or NoneType data, this may be 0/1 or several string
    variations such as T/F, Y/N (case insensitive) also longform (yes/no) etc..

    Parameters
    ----------
    x : `str` or `NoneType`
        The value to parse.

    Returns
    -------
    x : `bool` or `NoneType`
        The parsed Boolean.
    """
    if x is None:
        return x
    return parse_bool(x)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_date(x):
    """Parse date data. Several date formats are recognised.

    Parameters
    ----------
    x : `str`
        The value to parse.

    Returns
    -------
    x : `datetime.Date`
        The parsed date.
    """
    for i in STRING_DATES:
        try:
            return datetime.strptime(x, i)
        except ValueError:
            pass
    raise TypeError(f"unknown date: {x}")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_date_none(x):
    """Parse date or `NoneType` data. Several date formats are recognised.

    Parameters
    ----------
    x : `str`
        The value to parse.

    Returns
    -------
    x : `datetime.Date` or `NoneType`
        The parsed date or missing value.
    """
    if x is None:
        return x
    return parse_date(x)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_date_time(x):
    """Parse datetime data.

    Parameters
    ----------
    x : `str`
        The value to parse.

    Returns
    -------
    x : `datetime.DateTime`
        The parsed string, this will have leading or lagging whitespace
        removed.
    """
    for i in STRING_DATE_TIME:
        try:
            return datetime.strptime(x, i)
        except ValueError:
            pass
    raise TypeError(f"unknown datetime: {x}")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_date_time_none(x):
    """Parse datetime or ``NoneType`` data.

    Parameters
    ----------
    x : `str` or `NoneType`
        The value to parse.

    Returns
    -------
    x : `datetime.DateTime` or `NoneType`
        The parsed date time or `NoneType`.
    """
    if x is None:
        return x
    return parse_date_time(x)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
