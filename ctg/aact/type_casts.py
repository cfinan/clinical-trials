"""Ensure variables are the correct types
"""
from datetime import datetime
import re

DATES = [
    "%Y-%m-%d", "%B %d, %Y", "%B %Y", "%Y-%m-%d %H:%M:%s.%f"
]

DAYS_IN = dict(day=1, month=30, year=365)
DURATION = r"(?P<NUMBER>\d+) (?P<TIME>day|month|year)s?"
BOOL = dict(f=False, t=True)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_ncd_id(value):
    """
    """
    if not re.match(r'NCT\d{8}'):
        raise ValueError("wrong NCD ID: {0}".format(value))
    return value


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_duration(value):
    """
    """
    match = re.match(DURATION, value)

    try:
        x = int(match.group('NUMBER'))
        return x * DAYS_IN[match.group('TIME')]
    except AttributeError:
        return None


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_date(value):
    """
    """
    for i in DATES:
        return datetime.strptime(i)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_phase(value):
    """
    """
    phases = []
    for i in value.lower().split('/'):
        phases.append(int(re.sub(r'phase (\d+)', '\1', i.strip())))

    try:
        return sum(phases)/len(phases)
    except ZeroDivisionError:
        return None


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_text(value):
    """
    """
    return re.sub(r'[\r\n]', ' ', value)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_bool(value):
    """
    """
    return BOOL[value]
