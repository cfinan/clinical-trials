"""SQLAlchemy ORM module.
"""
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (
    Boolean,
    Column,
    Date,
    Float,
    ForeignKey,
    Integer,
    Sequence,
    String,
    Text
)
from sqlalchemy.orm import relationship

Base = declarative_base()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def print_orm_obj(obj, exclude=[]):
    """Prints the contents of the SQL Alchemy ORM object.

    Parameters
    ----------
    obj : `sqlalchemy.ext.declarative.Base`
        The SQLAlchemy declarative base model object. This should have a
        __table__ attribute that contains the corresponding SQLAlchemy table
        object.
    exclude : `NoneType` or `list` or `str`, optional, default `NoneType`
        A list of attributes (these correspond to table columns) that you do
        not want to include in the string.

    Returns
    -------
    formatted_string : `str`
        A formatted string that can be printed. The string contains the
        attributes of the object and the data they contain.

    Notes
    -----
    This is designed to be called from an SQLAlchemy ORM model object and it
    in the __repr__ method. It simply frovides a formatted string that has the
    data in the object.
    """
    exclude = exclude or []

    attr_values = []
    for i in obj.__table__.columns:
        if i.name in exclude:
            continue
        val = getattr(obj, i.name)
        if val.__class__.__name__ != "method":
            attr_values.append("{0}={1}".format(i.name, val))

    # Turn outlist into a string and return
    return "<{0}({1})>".format(obj.__class__.__name__, ", ".join(attr_values))


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BaselineCount(Base):
    """A representation of the ``baseline_counts`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    result_group_id : `int`
        The ``result_group_id`` column. Foreign key to ``result_groups.id``.
    ctgov_group_code : `str`
        The ``ctgov_group_code`` column (length 20).
    units : `str`
        The ``units`` column (length 60).
    scope : `str`
        The ``scope`` column (length 20).
    count : `int`, optional, default: `NoneType`
        The ``count`` column.
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.
    result_groups : `clinical_trials.orm.ResultGroup`, optional, default: \
    `NoneType`
        Relationship with ``result_groups``.

    Notes
    -----
    Sample size at baseline for each study group; usually a count of
    participants but can represent other units of measure such as 'hands',
    'hips', etc. This is part of the ``ctgov`` schema. This is part of the
    Results domain. This maps to `many` rows in the ``studies`` table. See
    `Result_Baseline_ArmGroup_numUnitsAnalyzed
    <https://prsinfo.clinicaltrials.gov/results_definitions.html>`_ for
    documentation.
    """
    __tablename__ = "baseline_counts"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    result_group_id = Column(
        Integer, ForeignKey("result_groups.id"), index=True, unique=False,
        nullable=False
    )
    ctgov_group_code = Column(
        String(20), index=False, unique=False, nullable=False
    )
    units = Column(
        String(60), index=False, unique=False, nullable=False
    )
    scope = Column(
        String(20), index=False, unique=False, nullable=False
    )
    count = Column(
        Integer, index=False, unique=False, nullable=True
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="baseline_counts",
        doc="Relationship back to ``studies``"
    )
    result_groups = relationship(
        "ResultGroup", back_populates="baseline_counts",
        doc="Relationship back to ``result_groups``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BaselineMeasurement(Base):
    """A representation of the ``baseline_measurements`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    result_group_id : `int`
        The ``result_group_id`` column. Foreign key to ``result_groups.id``.
    ctgov_group_code : `str`
        The ``ctgov_group_code`` column (length 20).
    classification : `str`, optional, default: `NoneType`
        The ``classification`` column (length 120).
    category : `str`, optional, default: `NoneType`
        The ``category`` column (length 120).
    title : `str`
        The ``title`` column (length 120).
    description : `str`, optional, default: `NoneType`
        The ``description`` column (length 660).
    units : `str`, optional, default: `NoneType`
        The ``units`` column (length 60).
    param_type : `str`, optional, default: `NoneType`
        The ``param_type`` column (length 40).
    param_value : `str`
        value provided as string (length 20).
    param_value_num : `float`, optional, default: `NoneType`
        value provided as decimal.
    dispersion_type : `str`, optional, default: `NoneType`
        The ``dispersion_type`` column (length 40).
    dispersion_value : `str`, optional, default: `NoneType`
        value provided as string (length 20).
    dispersion_value_num : `float`, optional, default: `NoneType`
        value provided as decimal.
    dispersion_lower_limit : `float`, optional, default: `NoneType`
        Used for reporting the lower limit of the interquartile range or full
        range.
    dispersion_upper_limit : `float`, optional, default: `NoneType`
        Used for reporting the upper limit of the interquartile range or full
        range.
    explanation_of_na : `str`, optional, default: `NoneType`
        The ``explanation_of_na`` column (length 560).
    number_analyzed : `int`, optional, default: `NoneType`
        The ``number_analyzed`` column.
    number_analyzed_units : `str`, optional, default: `NoneType`
        The ``number_analyzed_units`` column (length 40).
    population_description : `str`, optional, default: `NoneType`
        The ``population_description`` column (length 560).
    calculate_percentage : `bool`, optional, default: `NoneType`
        The ``calculate_percentage`` column.
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.
    result_groups : `clinical_trials.orm.ResultGroup`, optional, default: \
    `NoneType`
        Relationship with ``result_groups``.

    Notes
    -----
    Summaries of demographic & baseline measures collected by arm or comparison
    group and for the entire population of participants in the clinical study.
    This is part of the ``ctgov`` schema. This is part of the Results domain.
    This maps to `many` rows in the ``studies`` table.
    """
    __tablename__ = "baseline_measurements"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    result_group_id = Column(
        Integer, ForeignKey("result_groups.id"), index=True, unique=False,
        nullable=False
    )
    ctgov_group_code = Column(
        String(20), index=False, unique=False, nullable=False
    )
    classification = Column(
        String(120), index=False, unique=False, nullable=True
    )
    category = Column(
        String(120), index=False, unique=False, nullable=True
    )
    title = Column(
        String(120), index=False, unique=False, nullable=False
    )
    description = Column(
        String(660), index=False, unique=False, nullable=True
    )
    units = Column(
        String(60), index=False, unique=False, nullable=True
    )
    param_type = Column(
        String(40), index=False, unique=False, nullable=True
    )
    param_value = Column(
        String(20), index=False, unique=False, nullable=False,
        doc="value provided as string."
    )
    param_value_num = Column(
        Float, index=False, unique=False, nullable=True,
        doc="value provided as decimal."
    )
    dispersion_type = Column(
        String(40), index=False, unique=False, nullable=True
    )
    dispersion_value = Column(
        String(20), index=False, unique=False, nullable=True,
        doc="value provided as string."
    )
    dispersion_value_num = Column(
        Float, index=False, unique=False, nullable=True,
        doc="value provided as decimal."
    )
    dispersion_lower_limit = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Used for reporting the lower limit of the interquartile range or"
        " full range."
    )
    dispersion_upper_limit = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Used for reporting the upper limit of the interquartile range or"
        " full range."
    )
    explanation_of_na = Column(
        String(560), index=False, unique=False, nullable=True
    )
    number_analyzed = Column(
        Integer, index=False, unique=False, nullable=True
    )
    number_analyzed_units = Column(
        String(40), index=False, unique=False, nullable=True
    )
    population_description = Column(
        String(560), index=False, unique=False, nullable=True
    )
    calculate_percentage = Column(
        Boolean, index=False, unique=False, nullable=True
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="baseline_measurements",
        doc="Relationship back to ``studies``"
    )
    result_groups = relationship(
        "ResultGroup", back_populates="baseline_measurements",
        doc="Relationship back to ``result_groups``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BriefSummary(Base):
    """A representation of the ``brief_summaries`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    description : `str`
        The ``description`` column (length 5500).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    A single text column that provides a brief description of the study. This
    is part of the ``ctgov`` schema. This is part of the Protocol domain. This
    maps to `one` row in the ``studies`` table. See `BriefSummary
    <https://prsinfo.clinicaltrials.gov/definitions.html>`_ for documentation.
    """
    __tablename__ = "brief_summaries"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    description = Column(
        String(5500), index=False, unique=False, nullable=False
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="brief_summaries",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BrowseCondition(Base):
    """A representation of the ``browse_conditions`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    mesh_term : `str`
        Condition MeSH terms generated by NLM algorithm (length 80).
    downcase_mesh_term : `str`
        The ``downcase_mesh_term`` column (length 80).
    mesh_type : `str`
        The ``mesh_type`` column (length 20).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    NLM uses an internal algorithm to assess the data entered for a study and
    creates a list of standard MeSH terms that describe the condition(s) being
    addressed by the clinical trial. This table provides the results of NLM's
    assessment. This is part of the ``ctgov`` schema. This is part of the
    Protocol domain. This maps to `many` rows in the ``studies`` table.
    """
    __tablename__ = "browse_conditions"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    mesh_term = Column(
        String(80), index=False, unique=False, nullable=False,
        doc="Condition MeSH terms generated by NLM algorithm."
    )
    downcase_mesh_term = Column(
        String(80), index=False, unique=False, nullable=False
    )
    mesh_type = Column(
        String(20), index=False, unique=False, nullable=False
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="browse_conditions",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BrowseIntervention(Base):
    """A representation of the ``browse_interventions`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    mesh_term : `str`
        Intervention MeSH terms generated by NLM algorithm (length 220).
    downcase_mesh_term : `str`
        The ``downcase_mesh_term`` column (length 220).
    mesh_type : `str`
        The ``mesh_type`` column (length 20).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    NLM uses an internal algorithm to assess the data entered for a study and
    creates a list of standard MeSH terms that describe the intervention(s)
    being addressed by the clinical trial. This table provides the results of
    NLM's assessment. This is part of the ``ctgov`` schema. This is part of the
    Protocol domain. This maps to `many` rows in the ``studies`` table.
    """
    __tablename__ = "browse_interventions"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    mesh_term = Column(
        String(220), index=False, unique=False, nullable=False,
        doc="Intervention MeSH terms generated by NLM algorithm."
    )
    downcase_mesh_term = Column(
        String(220), index=False, unique=False, nullable=False
    )
    mesh_type = Column(
        String(20), index=False, unique=False, nullable=False
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="browse_interventions",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CalculatedValue(Base):
    """A representation of the ``calculated_values`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    number_of_facilities : `int`, optional, default: `NoneType`
        count number of related facility rows.
    number_of_nsae_subjects : `int`, optional, default: `NoneType`
        count number of related non-serious event rows.
    number_of_sae_subjects : `int`, optional, default: `NoneType`
        count number of related serious event rows.
    registered_in_calendar_year : `int`
        Year value of first_received_date which represents the year in which
        the study was registered.
    nlm_download_date : `str`, optional, default: `NoneType`
        Date NLM made study available via their API (length 255).
    actual_duration : `int`, optional, default: `NoneType`
        Number of months between start_date & primary_completion_date.
    were_results_reported : `bool`
        Set to true if clinical_results exist.
    months_to_report_results : `int`, optional, default: `NoneType`
        Number of months between primary_completion_date &
        first_received_results_date.
    has_us_facility : `bool`, optional, default: `NoneType`
        Set to true if the study has at least one facility in the USA. It is
        possible that some studies with sites in the USA will not be set to
        true if the study has not identified the facilities.
    has_single_facility : `bool`
        Set to true if the study has just one facility.
    minimum_age_num : `int`, optional, default: `NoneType`
        convert minimum age to an integer.
    maximum_age_num : `int`, optional, default: `NoneType`
        convert maximum age to an integer.
    minimum_age_unit : `str`, optional, default: `NoneType`
        part of minimum age info that specifies units (length 20).
    maximum_age_unit : `str`, optional, default: `NoneType`
        part of maximum age info that specifies units (length 20).
    number_of_primary_outcomes_to_measure : `int`, optional, default: \
    `NoneType`
        count number of primary outcomes that the investigator intends to
        measure.
    number_of_secondary_outcomes_to_measure : `int`, optional, default: \
    `NoneType`
        count number of secondary outcomes that the investigator intends to
        measure.
    number_of_other_outcomes_to_measure : `int`, optional, default: \
    `NoneType`
        count number of other outcomes that the investigator intends to
        measure.
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    An AACT-provided table that contains info that's been calculated from the
    information received from ClinicalTrials.gov. For example,
    number_of_facilities and actual_duration are provided in this table. This
    is part of the ``ctgov`` schema. This is part of the Protocol domain. This
    maps to `one` row in the ``studies`` table.
    """
    __tablename__ = "calculated_values"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    number_of_facilities = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="count number of related facility rows."
    )
    number_of_nsae_subjects = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="count number of related non-serious event rows."
    )
    number_of_sae_subjects = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="count number of related serious event rows."
    )
    registered_in_calendar_year = Column(
        Integer, index=False, unique=False, nullable=False,
        doc="Year value of first_received_date which represents the year in "
        "which the study was registered."
    )
    nlm_download_date = Column(
        String(255), index=False, unique=False, nullable=True,
        doc="Date NLM made study available via their API."
    )
    actual_duration = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="Number of months between start_date & primary_completion_date."
    )
    were_results_reported = Column(
        Boolean, index=False, unique=False, nullable=False,
        doc="Set to true if clinical_results exist."
    )
    months_to_report_results = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="Number of months between primary_completion_date & "
        "first_received_results_date."
    )
    has_us_facility = Column(
        Boolean, index=False, unique=False, nullable=True,
        doc="Set to true if the study has at least one facility in the USA. "
        "It is possible that some studies with sites in the USA will not be "
        "set to true if the study has not identified the facilities."
    )
    has_single_facility = Column(
        Boolean, index=False, unique=False, nullable=False,
        doc="Set to true if the study has just one facility."
    )
    minimum_age_num = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="convert minimum age to an integer."
    )
    maximum_age_num = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="convert maximum age to an integer."
    )
    minimum_age_unit = Column(
        String(20), index=False, unique=False, nullable=True,
        doc="part of minimum age info that specifies units."
    )
    maximum_age_unit = Column(
        String(20), index=False, unique=False, nullable=True,
        doc="part of maximum age info that specifies units."
    )
    number_of_primary_outcomes_to_measure = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="count number of primary outcomes that the investigator intends "
        "to measure."
    )
    number_of_secondary_outcomes_to_measure = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="count number of secondary outcomes that the investigator intends"
        " to measure."
    )
    number_of_other_outcomes_to_measure = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="count number of other outcomes that the investigator intends to "
        "measure."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="calculated_values",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CentralContact(Base):
    """A representation of the ``central_contacts`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    contact_type : `str`
        'backup' if value from <overall_contact_backup> 'regular' if value from
        <overall_contact> (length 20).
    name : `str`
        The ``name`` column (length 160).
    phone : `str`, optional, default: `NoneType`
        The ``phone`` column (length 40).
    email : `str`, optional, default: `NoneType`
        The ``email`` column (length 100).
    phone_extension : `str`, optional, default: `NoneType`
        The ``phone_extension`` column (length 20).
    role : `str`
        The ``role`` column (length 20).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    Contact info for people (primary & backup) who can answer questions
    concerning enrollment at any location of the study. This is part of the
    ``ctgov`` schema. This is part of the Protocol domain. This maps to `many`
    rows in the ``studies`` table.
    """
    __tablename__ = "central_contacts"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    contact_type = Column(
        String(20), index=False, unique=False, nullable=False,
        doc="'backup' if value from <overall_contact_backup> 'regular' if "
        "value from <overall_contact>."
    )
    name = Column(
        String(160), index=False, unique=False, nullable=False
    )
    phone = Column(
        String(40), index=False, unique=False, nullable=True
    )
    email = Column(
        String(100), index=False, unique=False, nullable=True
    )
    phone_extension = Column(
        String(20), index=False, unique=False, nullable=True
    )
    role = Column(
        String(20), index=False, unique=False, nullable=False
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="central_contacts",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Condition(Base):
    """A representation of the ``conditions`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    name : `str`
        The ``name`` column (length 180).
    downcase_name : `str`
        The ``downcase_name`` column (length 180).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    Name(s) of the disease(s) or condition(s) studied in the clinical study, or
    the focus of the clinical study. Can include NLM's Medical Subject Heading
    (MeSH)-controlled vocabulary terms. This is part of the ``ctgov`` schema.
    This is part of the Protocol domain. This maps to `many` rows in the
    ``studies`` table.
    """
    __tablename__ = "conditions"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    name = Column(
        String(180), index=False, unique=False, nullable=False
    )
    downcase_name = Column(
        String(180), index=False, unique=False, nullable=False
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="conditions",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Country(Base):
    """A representation of the ``countries`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    name : `str`, optional, default: `NoneType`
        The ``name`` column (length 60).
    removed : `bool`
        True if value from removed_countries.
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    Countries in which the study has facilities/sites. This is part of the
    ``ctgov`` schema. This is part of the Protocol domain. This maps to `many`
    rows in the ``studies`` table.
    """
    __tablename__ = "countries"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    name = Column(
        String(60), index=False, unique=False, nullable=True
    )
    removed = Column(
        Boolean, index=False, unique=False, nullable=False,
        doc="True if value from removed_countries."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="countries",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DesignGroupIntervention(Base):
    """A representation of the ``design_group_interventions`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    design_group_id : `int`
        The ``design_group_id`` column. Foreign key to ``design_groups.id``.
    intervention_id : `int`
        The ``intervention_id`` column. Foreign key to ``interventions.id``.
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.
    design_groups : `clinical_trials.orm.DesignGroup`, optional, default: \
    `NoneType`
        Relationship with ``design_groups``.
    interventions : `clinical_trials.orm.Intervention`, optional, default: \
    `NoneType`
        Relationship with ``interventions``.

    Notes
    -----
    A cross reference for groups/interventions. If a study has multiple groups
    and multiple interventions, this table shows which interventions are
    associated with which groups. This is part of the ``ctgov`` schema. This is
    part of the Protocol domain. This maps to `many` rows in the ``studies``
    table. See `crossref
    <https://prsinfo.clinicaltrials.gov/definitions.html>`_ for documentation.
    """
    __tablename__ = "design_group_interventions"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    design_group_id = Column(
        Integer, ForeignKey("design_groups.id"), index=True, unique=False,
        nullable=False
    )
    intervention_id = Column(
        Integer, ForeignKey("interventions.id"), index=True, unique=False,
        nullable=False
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="design_group_interventions",
        doc="Relationship back to ``studies``"
    )
    design_groups = relationship(
        "DesignGroup", back_populates="design_group_interventions",
        doc="Relationship back to ``design_groups``"
    )
    interventions = relationship(
        "Intervention", back_populates="design_group_interventions",
        doc="Relationship back to ``interventions``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DesignGroup(Base):
    """A representation of the ``design_groups`` table.

    Parameters
    ----------
    id : `int`
        The Design_Groups table contains info for study arms/groups that are
        provided when the study is registered.This table does not include
        results info (see Result_Groups table.).
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    group_type : `str`, optional, default: `NoneType`
        The ``group_type`` column (length 20).
    title : `str`
        This tag is available only of those studies that have results and
        indicates the date when the results were first received. This field
        captures the arm label or the group cohort label for the records coming
        from study tables, and arm/group title for the results tables (length
        120).
    description : `str`, optional, default: `NoneType`
        The ``description`` column (length 1100).
    design_group_interventions : \
    `clinical_trials.orm.DesignGroupIntervention`, optional, default: \
    `NoneType`
        Relationship with ``design_group_interventions``.
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    Defines the protocol-specified group, subgroup, or cohort of participants
    in a clinical trial assigned to receive specific intervention(s) or
    observations according to a protocol. This is part of the ``ctgov`` schema.
    This is part of the Protocol domain. This maps to `many` rows in the
    ``studies`` table. See `ArmsGroupsInterventionsLabel
    <https://prsinfo.clinicaltrials.gov/definitions.html>`_ for documentation.
    """
    __tablename__ = "design_groups"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="The Design_Groups table contains info for study arms/groups that"
        " are provided when the study is registered.This table does not "
        "include results info (see Result_Groups table.)."
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    group_type = Column(
        String(20), index=False, unique=False, nullable=True
    )
    title = Column(
        String(120), index=False, unique=False, nullable=False,
        doc="This tag is available only of those studies that have results "
        "and indicates the date when the results were first received. This "
        "field captures the arm label or the group cohort label for the "
        "records coming from study tables, and arm/group title for the results"
        " tables."
    )
    description = Column(
        String(1100), index=False, unique=False, nullable=True
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    design_group_interventions = relationship(
        "DesignGroupIntervention", back_populates="design_groups",
        doc="Relationship back to ``design_group_interventions``."
    )
    studies = relationship(
        "Study", back_populates="design_groups",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DesignOutcome(Base):
    """A representation of the ``design_outcomes`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    outcome_type : `str`
        primary' if value from <primary_outcome> 'secondary' if value from
        <secondary_outcome> (length 20).
    measure : `str`, optional, default: `NoneType`
        The ``measure`` column (length 300).
    time_frame : `str`, optional, default: `NoneType`
        The ``time_frame`` column (length 300).
    population : `str`, optional, default: `NoneType`
        The ``population`` column (length 255).
    description : `str`, optional, default: `NoneType`
        The ``description`` column (length 1100).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    Description of planned outcome measures and observations that will describe
    patterns of diseases and traits/associations with exposures, risk factors
    or treatment. This is part of the ``ctgov`` schema. This is part of the
    Protocol domain. This maps to `many` rows in the ``studies`` table. See
    `OutcomesBody <https://prsinfo.clinicaltrials.gov/definitions.html>`_ for
    documentation.
    """
    __tablename__ = "design_outcomes"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    outcome_type = Column(
        String(20), index=False, unique=False, nullable=False,
        doc="primary' if value from <primary_outcome> 'secondary' if value "
        "from <secondary_outcome>."
    )
    measure = Column(
        String(300), index=False, unique=False, nullable=True
    )
    time_frame = Column(
        String(300), index=False, unique=False, nullable=True
    )
    population = Column(
        String(255), index=False, unique=False, nullable=True
    )
    description = Column(
        String(1100), index=False, unique=False, nullable=True
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="design_outcomes",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Design(Base):
    """A representation of the ``designs`` table.

    Parameters
    ----------
    id : `int`
        Many values in this table are derived Per NLM's data element
        <study_design>.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    allocation : `str`, optional, default: `NoneType`
        The ``allocation`` column (length 20).
    intervention_model : `str`, optional, default: `NoneType`
        The ``intervention_model`` column (length 40).
    observational_model : `str`, optional, default: `NoneType`
        The ``observational_model`` column (length 40).
    primary_purpose : `str`, optional, default: `NoneType`
        The ``primary_purpose`` column (length 40).
    time_perspective : `str`, optional, default: `NoneType`
        The ``time_perspective`` column (length 20).
    masking : `str`, optional, default: `NoneType`
        The ``masking`` column (length 20).
    masking_description : `str`, optional, default: `NoneType`
        The ``masking_description`` column (length 1100).
    intervention_model_description : `str`, optional, default: `NoneType`
        The ``intervention_model_description`` column (length 1100).
    subject_masked : `bool`, optional, default: `NoneType`
        Parse content of 'Masking:' section. If it includes 'Subject' set to
        true.
    caregiver_masked : `bool`, optional, default: `NoneType`
        Parse content of 'Masking:' section. If it includes 'Caregiver' set to
        true.
    investigator_masked : `bool`, optional, default: `NoneType`
        Parse content of 'Masking:' section. If it includes 'Investigator' set
        to true.
    outcomes_assessor_masked : `bool`, optional, default: `NoneType`
        Parse content of 'Masking:' section. If it includes 'Outcomes_Assessor'
        set to true.
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    Description of how the study will be conducted, including comparison group
    design and strategies for masking and allocating participants. This is part
    of the ``ctgov`` schema. This is part of the Protocol domain. This maps to
    `one` row in the ``studies`` table. See `StudyDesignLabel
    <https://prsinfo.clinicaltrials.gov/definitions.html>`_ for documentation.
    """
    __tablename__ = "designs"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Many values in this table are derived Per NLM's data element "
        "<study_design>."
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    allocation = Column(
        String(20), index=False, unique=False, nullable=True
    )
    intervention_model = Column(
        String(40), index=False, unique=False, nullable=True
    )
    observational_model = Column(
        String(40), index=False, unique=False, nullable=True
    )
    primary_purpose = Column(
        String(40), index=False, unique=False, nullable=True
    )
    time_perspective = Column(
        String(20), index=False, unique=False, nullable=True
    )
    masking = Column(
        String(20), index=False, unique=False, nullable=True
    )
    masking_description = Column(
        String(1100), index=False, unique=False, nullable=True
    )
    intervention_model_description = Column(
        String(1100), index=False, unique=False, nullable=True
    )
    subject_masked = Column(
        Boolean, index=False, unique=False, nullable=True,
        doc="Parse content of 'Masking:' section. If it includes 'Subject' "
        "set to true."
    )
    caregiver_masked = Column(
        Boolean, index=False, unique=False, nullable=True,
        doc="Parse content of 'Masking:' section. If it includes 'Caregiver' "
        "set to true."
    )
    investigator_masked = Column(
        Boolean, index=False, unique=False, nullable=True,
        doc="Parse content of 'Masking:' section. If it includes "
        "'Investigator' set to true."
    )
    outcomes_assessor_masked = Column(
        Boolean, index=False, unique=False, nullable=True,
        doc="Parse content of 'Masking:' section. If it includes "
        "'Outcomes_Assessor' set to true."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="designs",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DetailedDescription(Base):
    """A representation of the ``detailed_descriptions`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    description : `str`
        The ``description`` column (length 35040).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    A single text column that provides a detailed description of the study
    protocol. This is part of the ``ctgov`` schema. This is part of the
    Protocol domain. This maps to `one` row in the ``studies`` table. See
    `DetailedDescription
    <https://prsinfo.clinicaltrials.gov/definitions.html>`_ for documentation.
    """
    __tablename__ = "detailed_descriptions"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    description = Column(
        Text, index=False, unique=False, nullable=False
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="detailed_descriptions",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Document(Base):
    """A representation of the ``documents`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    document_id : `str`, optional, default: `NoneType`
        The ``document_id`` column (length 40).
    document_type : `str`
        The ``document_type`` column (length 220).
    url : `str`
        The ``url`` column (length 980).
    comment : `str`, optional, default: `NoneType`
        The ``comment`` column (length 1100).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    The full study protocol and statistical analysis plan must be uploaded as
    part of results information submission, for studies with a Primary
    Completion Date on or after January 18, 2017. The protocol and statistical
    analysis plan may be optionally uploaded before results information
    submission and updated with new versions, as needed. Informed consent forms
    may optionally be uploaded at any time. This is part of the ``ctgov``
    schema. This is part of the Results domain. This maps to `many` rows in the
    ``studies`` table. See `DocumentUpload
    <https://prsinfo.clinicaltrials.gov/results_definitions.html>`_ for
    documentation.
    """
    __tablename__ = "documents"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    document_id = Column(
        String(40), index=False, unique=False, nullable=True
    )
    document_type = Column(
        String(220), index=False, unique=False, nullable=False
    )
    url = Column(
        String(980), index=False, unique=False, nullable=False
    )
    comment = Column(
        String(1100), index=False, unique=False, nullable=True
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="documents",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class DropWithdrawal(Base):
    """A representation of the ``drop_withdrawals`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    result_group_id : `int`
        The ``result_group_id`` column. Foreign key to ``result_groups.id``.
    ctgov_group_code : `str`
        The ``ctgov_group_code`` column (length 20).
    period : `str`
        The ``period`` column (length 60).
    reason : `str`
        The ``reason`` column (length 120).
    count : `int`, optional, default: `NoneType`
        The ``count`` column.
    drop_withdraw_comment : `str`, optional, default: `NoneType`
        The ``drop_withdraw_comment`` column (length 255).
    reason_comment : `str`, optional, default: `NoneType`
        The ``reason_comment`` column (length 255).
    count_units : `str`, optional, default: `NoneType`
        The ``count_units`` column (length 255).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.
    result_groups : `clinical_trials.orm.ResultGroup`, optional, default: \
    `NoneType`
        Relationship with ``result_groups``.

    Notes
    -----
    Summarized information about how many participants withdrew from the study,
    when and why. This information explains disposition of participants
    relative to the numbers starting and completing the study (enumerated in
    the Milestones table). This is part of the ``ctgov`` schema. This is part
    of the Results domain. This maps to `many` rows in the ``studies`` table.
    See `NotCompleted
    <https://prsinfo.clinicaltrials.gov/results_definitions.html>`_ for
    documentation.
    """
    __tablename__ = "drop_withdrawals"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    result_group_id = Column(
        Integer, ForeignKey("result_groups.id"), index=True, unique=False,
        nullable=False
    )
    ctgov_group_code = Column(
        String(20), index=False, unique=False, nullable=False
    )
    period = Column(
        String(60), index=False, unique=False, nullable=False
    )
    reason = Column(
        String(120), index=False, unique=False, nullable=False
    )
    count = Column(
        Integer, index=False, unique=False, nullable=True
    )
    drop_withdraw_comment = Column(
        String(255), index=False, unique=False, nullable=True
    )
    reason_comment = Column(
        String(255), index=False, unique=False, nullable=True
    )
    count_units = Column(
        String(255), index=False, unique=False, nullable=True
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="drop_withdrawals",
        doc="Relationship back to ``studies``"
    )
    result_groups = relationship(
        "ResultGroup", back_populates="drop_withdrawals",
        doc="Relationship back to ``result_groups``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Eligibility(Base):
    """A representation of the ``eligibilities`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    sampling_method : `str`, optional, default: `NoneType`
        The ``sampling_method`` column (length 40).
    gender : `str`, optional, default: `NoneType`
        The ``gender`` column (length 20).
    minimum_age : `str`
        The ``minimum_age`` column (length 20).
    maximum_age : `str`
        The ``maximum_age`` column (length 20).
    healthy_volunteers : `str`, optional, default: `NoneType`
        The ``healthy_volunteers`` column (length 40).
    population : `str`, optional, default: `NoneType`
        The ``population`` column (length 1100).
    criteria : `str`, optional, default: `NoneType`
        The ``criteria`` column (length 21580).
    gender_description : `str`, optional, default: `NoneType`
        The ``gender_description`` column (length 1060).
    gender_based : `bool`, optional, default: `NoneType`
        The ``gender_based`` column.
    adult : `bool`
        The ``adult`` column.
    child : `bool`
        The ``child`` column.
    older_adult : `bool`
        The ``older_adult`` column.
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    Information about the criteria used to select participants; includes
    inclusion and exclusion criteria. This is part of the ``ctgov`` schema.
    This is part of the Protocol domain. This maps to `one` row in the
    ``studies`` table. See `EligibilityLabel
    <https://prsinfo.clinicaltrials.gov/definitions.html>`_ for documentation.
    """
    __tablename__ = "eligibilities"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    sampling_method = Column(
        String(40), index=False, unique=False, nullable=True
    )
    gender = Column(
        String(20), index=False, unique=False, nullable=True
    )
    minimum_age = Column(
        String(20), index=False, unique=False, nullable=False
    )
    maximum_age = Column(
        String(20), index=False, unique=False, nullable=False
    )
    healthy_volunteers = Column(
        String(40), index=False, unique=False, nullable=True
    )
    population = Column(
        String(1100), index=False, unique=False, nullable=True
    )
    criteria = Column(
        Text, index=False, unique=False, nullable=True
    )
    gender_description = Column(
        String(1060), index=False, unique=False, nullable=True
    )
    gender_based = Column(
        Boolean, index=False, unique=False, nullable=True
    )
    adult = Column(
        Boolean, index=False, unique=False, nullable=False
    )
    child = Column(
        Boolean, index=False, unique=False, nullable=False
    )
    older_adult = Column(
        Boolean, index=False, unique=False, nullable=False
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="eligibilities",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Facility(Base):
    """A representation of the ``facilities`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    status : `str`, optional, default: `NoneType`
        The ``status`` column (length 40).
    name : `str`, optional, default: `NoneType`
        The ``name`` column (length 300).
    city : `str`, optional, default: `NoneType`
        The ``city`` column (length 80).
    state : `str`, optional, default: `NoneType`
        The ``state`` column (length 80).
    zip : `str`, optional, default: `NoneType`
        The ``zip`` column (length 40).
    country : `str`, optional, default: `NoneType`
        The ``country`` column (length 60).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.
    facility_contacts : `clinical_trials.orm.FacilityContact`, optional, \
    default: `NoneType`
        Relationship with ``facility_contacts``.
    facility_investigators : `clinical_trials.orm.FacilityInvestigator`, \
    optional, default: `NoneType`
        Relationship with ``facility_investigators``.

    Notes
    -----
    Name, address and recruiting status of the facilities participating in the
    study. This is part of the ``ctgov`` schema. This is part of the Protocol
    domain. This maps to `many` rows in the ``studies`` table. See `Facility
    <https://prsinfo.clinicaltrials.gov/definitions.html>`_ for documentation.
    """
    __tablename__ = "facilities"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    status = Column(
        String(40), index=False, unique=False, nullable=True
    )
    name = Column(
        String(300), index=False, unique=False, nullable=True
    )
    city = Column(
        String(80), index=False, unique=False, nullable=True
    )
    state = Column(
        String(80), index=False, unique=False, nullable=True
    )
    zip = Column(
        String(40), index=False, unique=False, nullable=True
    )
    country = Column(
        String(60), index=False, unique=False, nullable=True
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="facilities",
        doc="Relationship back to ``studies``"
    )
    facility_contacts = relationship(
        "FacilityContact", back_populates="facilities",
        doc="Relationship back to ``facility_contacts``."
    )
    facility_investigators = relationship(
        "FacilityInvestigator", back_populates="facilities",
        doc="Relationship back to ``facility_investigators``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class FacilityContact(Base):
    """A representation of the ``facility_contacts`` table.

    Parameters
    ----------
    id : `int`
        Facility contact information is available if the facility status
        (Facilities.Status) is ‘Recruiting’ or ‘Not yet recruiting’, and if the
        data provider has provided such information. Contact information is
        removed from the publicly available content at ClinicalTrials.gov when
        the facility is no longer recruiting, or when the overall study status
        (Studies.Overall_status) changes to indicate that the study has
        completed recruitment.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    facility_id : `int`
        The ``facility_id`` column. Foreign key to ``facilities.id``.
    contact_type : `str`
        primary' if from <contact> 'backup' if from <contact_backup> (length
        20).
    name : `str`, optional, default: `NoneType`
        The ``name`` column (length 140).
    email : `str`, optional, default: `NoneType`
        The ``email`` column (length 100).
    phone : `str`, optional, default: `NoneType`
        The ``phone`` column (length 40).
    phone_extension : `str`, optional, default: `NoneType`
        The ``phone_extension`` column (length 20).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.
    facilities : `clinical_trials.orm.Facility`, optional, default: \
    `NoneType`
        Relationship with ``facilities``.

    Notes
    -----
    Contact information for people responsible for the study at each facility.
    (primary and backup) Facility contact information is available if the
    facility status (Facilities.Status) is ‘Recruiting’ or ‘Not yet
    recruiting’, and if the data provider has provided such information.
    Contact information is removed from the publicly available content at
    ClinicalTrials.gov when the facility is no longer recruiting, or when the
    overall study status (Studies.Overall_status) changes to indicate that the
    study has completed recruitment. This is part of the ``ctgov`` schema. This
    is part of the Protocol domain. This maps to `many` rows in the ``studies``
    table. See `FacilityContact
    <https://prsinfo.clinicaltrials.gov/definitions.html>`_ for documentation.
    """
    __tablename__ = "facility_contacts"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Facility contact information is available if the facility status"
        " (Facilities.Status) is ‘Recruiting’ or ‘Not yet recruiting’, and if "
        "the data provider has provided such information. Contact information "
        "is removed from the publicly available content at ClinicalTrials.gov "
        "when the facility is no longer recruiting, or when the overall study "
        "status (Studies.Overall_status) changes to indicate that the study "
        "has completed recruitment."
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    facility_id = Column(
        Integer, ForeignKey("facilities.id"), index=True, unique=False,
        nullable=False
    )
    contact_type = Column(
        String(20), index=False, unique=False, nullable=False,
        doc="primary' if from <contact> 'backup' if from <contact_backup>."
    )
    name = Column(
        String(140), index=False, unique=False, nullable=True
    )
    email = Column(
        String(100), index=False, unique=False, nullable=True
    )
    phone = Column(
        String(40), index=False, unique=False, nullable=True
    )
    phone_extension = Column(
        String(20), index=False, unique=False, nullable=True
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="facility_contacts",
        doc="Relationship back to ``studies``"
    )
    facilities = relationship(
        "Facility", back_populates="facility_contacts",
        doc="Relationship back to ``facilities``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class FacilityInvestigator(Base):
    """A representation of the ``facility_investigators`` table.

    Parameters
    ----------
    id : `int`
        Facility investigator information is available if the facility status
        (Facilities.Status) is ‘Recruiting’ or ‘Not yet recruiting’, and if the
        data provider has provided such information. Investigator information
        is removed from the publicly available content at ClinicalTrials.gov
        when the facility is no longer recruiting, or when the overall study
        status (Studies.Overall_status) changes to indicate that the study has
        completed recruitment.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    facility_id : `int`
        The ``facility_id`` column. Foreign key to ``facilities.id``.
    role : `str`
        The ``role`` column (length 40).
    name : `str`
        The ``name`` column (length 120).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.
    facilities : `clinical_trials.orm.Facility`, optional, default: \
    `NoneType`
        Relationship with ``facilities``.

    Notes
    -----
    Names of the investigators at each study facility. Facility investigator
    information is available if the facility status (Facilities.Status) is
    ‘Recruiting’ or ‘Not yet recruiting’, and if the data provider has provided
    such information. Investigator information is removed from the publicly
    available content at ClinicalTrials.gov when the facility is no longer
    recruiting, or when the overall study status (Studies.Overall_status)
    changes to indicate that the study has completed recruitment. This is part
    of the ``ctgov`` schema. This is part of the Protocol domain. This maps to
    `many` rows in the ``studies`` table. See `StudyOfficials
    <https://prsinfo.clinicaltrials.gov/definitions.html>`_ for documentation.
    """
    __tablename__ = "facility_investigators"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="Facility investigator information is available if the facility "
        "status (Facilities.Status) is ‘Recruiting’ or ‘Not yet recruiting’, "
        "and if the data provider has provided such information. Investigator "
        "information is removed from the publicly available content at "
        "ClinicalTrials.gov when the facility is no longer recruiting, or when"
        " the overall study status (Studies.Overall_status) changes to "
        "indicate that the study has completed recruitment."
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    facility_id = Column(
        Integer, ForeignKey("facilities.id"), index=True, unique=False,
        nullable=False
    )
    role = Column(
        String(40), index=False, unique=False, nullable=False
    )
    name = Column(
        String(120), index=False, unique=False, nullable=False
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="facility_investigators",
        doc="Relationship back to ``studies``"
    )
    facilities = relationship(
        "Facility", back_populates="facility_investigators",
        doc="Relationship back to ``facilities``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class IdInformation(Base):
    """A representation of the ``id_information`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    id_source : `str`
        The ``id_source`` column (length 20).
    id_value : `str`
        The ``id_value`` column (length 40).
    id_type : `str`, optional, default: `NoneType`
        org_study_id' if value from <org_study_id> 'secondary_id' if value from
        <secondary_id> 'nct_id' if value from <nct_id> 'nct_alias' if value
        from <nct_alias> (length 40).
    id_type_description : `str`, optional, default: `NoneType`
        The ``id_type_description`` column (length 140).
    id_link : `str`, optional, default: `NoneType`
        The ``id_link`` column (length 80).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    Identifiers (other than the NCT ID) that uniquely identify the study such
    as that assigned by the sponsor, or an NCT ID that had previously been used
    for the study. This is part of the ``ctgov`` schema. This is part of the
    Protocol domain. This maps to `many` rows in the ``studies`` table. See
    `SecondaryIds <https://prsinfo.clinicaltrials.gov/definitions.html>`_ for
    documentation.
    """
    __tablename__ = "id_information"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    id_source = Column(
        String(20), index=False, unique=False, nullable=False
    )
    id_value = Column(
        String(40), index=False, unique=False, nullable=False
    )
    id_type = Column(
        String(40), index=False, unique=False, nullable=True,
        doc="org_study_id' if value from <org_study_id> 'secondary_id' if "
        "value from <secondary_id> 'nct_id' if value from <nct_id> 'nct_alias'"
        " if value from <nct_alias>."
    )
    id_type_description = Column(
        String(140), index=False, unique=False, nullable=True
    )
    id_link = Column(
        String(80), index=False, unique=False, nullable=True
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="id_information",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class InterventionOtherName(Base):
    """A representation of the ``intervention_other_names`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    intervention_id : `int`
        AACT assigned foreig.
    name : `str`
        The ``name`` column (length 240).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    Terns or phrases that are synonymous with an intervention. (Each row is
    linked to one of the interventions associated with the study.). This is
    part of the ``ctgov`` schema. This is part of the Protocol domain. This
    maps to `many` rows in the ``studies`` table. See `InterventionOtherName
    <https://prsinfo.clinicaltrials.gov/definitions.html>`_ for documentation.
    """
    __tablename__ = "intervention_other_names"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    intervention_id = Column(
        Integer, index=False, unique=False, nullable=False,
        doc="AACT assigned foreig."
    )
    name = Column(
        String(240), index=False, unique=False, nullable=False
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="intervention_other_names",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Intervention(Base):
    """A representation of the ``interventions`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    intervention_type : `str`
        The ``intervention_type`` column (length 40).
    name : `str`, optional, default: `NoneType`
        The ``name`` column (length 240).
    description : `str`, optional, default: `NoneType`
        The ``description`` column (length 1100).
    design_group_interventions : \
    `clinical_trials.orm.DesignGroupIntervention`, optional, default: \
    `NoneType`
        Relationship with ``design_group_interventions``.
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    The interventions or exposures (including drugs, medical devices,
    procedures, vaccines, and other products) of interest to the study, or
    associated with study arms/groups. This is part of the ``ctgov`` schema.
    This is part of the Protocol domain. This maps to `many` rows in the
    ``studies`` table. See `IntDesign
    <https://prsinfo.clinicaltrials.gov/definitions.html>`_ for documentation.
    """
    __tablename__ = "interventions"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    intervention_type = Column(
        String(40), index=False, unique=False, nullable=False
    )
    name = Column(
        String(240), index=False, unique=False, nullable=True
    )
    description = Column(
        String(1100), index=False, unique=False, nullable=True
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    design_group_interventions = relationship(
        "DesignGroupIntervention", back_populates="interventions",
        doc="Relationship back to ``design_group_interventions``."
    )
    studies = relationship(
        "Study", back_populates="interventions",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class IpdInformationType(Base):
    """A representation of the ``ipd_information_types`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    name : `str`
        The ``name`` column (length 40).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.
    """
    __tablename__ = "ipd_information_types"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    name = Column(
        String(40), index=False, unique=False, nullable=False
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="ipd_information_types",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Keyword(Base):
    """A representation of the ``keywords`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    name : `str`
        The ``name`` column (length 180).
    downcase_name : `str`
        The ``downcase_name`` column (length 180).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    Provides words or phrases that best describe the protocol. Keywords help
    users find studies in the database. Can include NLM's Medical Subject
    Heading (MeSH)-controlled vocabulary terms. This is part of the ``ctgov``
    schema. This is part of the Protocol domain. This maps to `many` rows in
    the ``studies`` table. See `Keywords
    <https://prsinfo.clinicaltrials.gov/definitions.html>`_ for documentation.
    """
    __tablename__ = "keywords"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    name = Column(
        String(180), index=False, unique=False, nullable=False
    )
    downcase_name = Column(
        String(180), index=False, unique=False, nullable=False
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="keywords",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Link(Base):
    """A representation of the ``links`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    url : `str`
        The ``url`` column (length 940).
    description : `str`
        The ``description`` column (length 280).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    Web site directly relevant to the protocol. (ie, links to educational,
    research, government, and other non-profit Web pages). This is part of the
    ``ctgov`` schema. This is part of the Protocol domain. This maps to `many`
    rows in the ``studies`` table. See `Links
    <https://prsinfo.clinicaltrials.gov/definitions.html>`_ for documentation.
    """
    __tablename__ = "links"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    url = Column(
        String(940), index=False, unique=False, nullable=False
    )
    description = Column(
        String(280), index=False, unique=False, nullable=False
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="links", doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Milestone(Base):
    """A representation of the ``milestones`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    result_group_id : `int`
        The ``result_group_id`` column. Foreign key to ``result_groups.id``.
    ctgov_group_code : `str`
        The ``ctgov_group_code`` column (length 20).
    title : `str`
        The ``title`` column (length 120).
    period : `str`
        The ``period`` column (length 60).
    description : `str`, optional, default: `NoneType`
        The ``description`` column (length 560).
    count : `int`, optional, default: `NoneType`
        The ``count`` column.
    milestone_description : `str`, optional, default: `NoneType`
        The ``milestone_description`` column (length 560).
    count_units : `str`, optional, default: `NoneType`
        The ``count_units`` column (length 20).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.
    result_groups : `clinical_trials.orm.ResultGroup`, optional, default: \
    `NoneType`
        Relationship with ``result_groups``.

    Notes
    -----
    Information summarizing the progress of participants through each stage of
    a study, including the number of participants who started and completed the
    trial. Enumeration of participants not completing the study is included in
    the Drop_Withdrawals table. This is part of the ``ctgov`` schema. This is
    part of the Results domain. This maps to `many` rows in the ``studies``
    table. See `MilestoneName
    <https://prsinfo.clinicaltrials.gov/results_definitions.html>`_ for
    documentation.
    """
    __tablename__ = "milestones"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    result_group_id = Column(
        Integer, ForeignKey("result_groups.id"), index=True, unique=False,
        nullable=False
    )
    ctgov_group_code = Column(
        String(20), index=False, unique=False, nullable=False
    )
    title = Column(
        String(120), index=False, unique=False, nullable=False
    )
    period = Column(
        String(60), index=False, unique=False, nullable=False
    )
    description = Column(
        String(560), index=False, unique=False, nullable=True
    )
    count = Column(
        Integer, index=False, unique=False, nullable=True
    )
    milestone_description = Column(
        String(560), index=False, unique=False, nullable=True
    )
    count_units = Column(
        String(20), index=False, unique=False, nullable=True
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="milestones",
        doc="Relationship back to ``studies``"
    )
    result_groups = relationship(
        "ResultGroup", back_populates="milestones",
        doc="Relationship back to ``result_groups``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class OutcomeAnalysis(Base):
    """A representation of the ``outcome_analyses`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    outcome_id : `int`
        The ``outcome_id`` column. Foreign key to ``outcomes.id``.
    non_inferiority_type : `str`
        The ``non_inferiority_type`` column (length 60).
    non_inferiority_description : `str`, optional, default: `NoneType`
        The ``non_inferiority_description`` column (length 560).
    param_type : `str`, optional, default: `NoneType`
        The ``param_type`` column (length 60).
    param_value : `float`, optional, default: `NoneType`
        The ``param_value`` column.
    dispersion_type : `str`, optional, default: `NoneType`
        The ``dispersion_type`` column (length 40).
    dispersion_value : `float`, optional, default: `NoneType`
        The ``dispersion_value`` column.
    p_value_modifier : `str`, optional, default: `NoneType`
        The p-value modifier: typically a less than or greater than sign
        (length 20).
    p_value : `float`, optional, default: `NoneType`
        Just the numeric part of the value provided - to 6-decimal place
        precision.
    ci_n_sides : `str`, optional, default: `NoneType`
        The ``ci_n_sides`` column (length 20).
    ci_percent : `float`, optional, default: `NoneType`
        The ``ci_percent`` column.
    ci_lower_limit : `float`, optional, default: `NoneType`
        The ``ci_lower_limit`` column.
    ci_upper_limit : `float`, optional, default: `NoneType`
        The ``ci_upper_limit`` column.
    ci_upper_limit_na_comment : `str`, optional, default: `NoneType`
        The ``ci_upper_limit_na_comment`` column (length 380).
    p_value_description : `str`, optional, default: `NoneType`
        The ``p_value_description`` column (length 280).
    method : `str`, optional, default: `NoneType`
        The ``method`` column (length 60).
    method_description : `str`, optional, default: `NoneType`
        The ``method_description`` column (length 180).
    estimate_description : `str`, optional, default: `NoneType`
        The ``estimate_description`` column (length 280).
    groups_description : `str`, optional, default: `NoneType`
        The ``groups_description`` column (length 560).
    other_analysis_description : `str`, optional, default: `NoneType`
        The ``other_analysis_description`` column (length 1100).
    ci_upper_limit_raw : `str`, optional, default: `NoneType`
        The ``ci_upper_limit_raw`` column (length 255).
    ci_lower_limit_raw : `str`, optional, default: `NoneType`
        The ``ci_lower_limit_raw`` column (length 255).
    p_value_raw : `str`, optional, default: `NoneType`
        The ``p_value_raw`` column (length 255).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.
    outcomes : `clinical_trials.orm.Outcome`, optional, default: `NoneType`
        Relationship with ``outcomes``.
    outcome_analysis_groups : `clinical_trials.orm.OutcomeAnalysisGroup`, \
    optional, default: `NoneType`
        Relationship with ``outcome_analysis_groups``.

    Notes
    -----
    Results of scientifically appropriate statistical analyses performed on
    primary and secondary study outcomes. Includes results for treatment effect
    estimates, confidence intervals and othe rmeasures of dispersion, and
    p-values. This is part of the ``ctgov`` schema. This is part of the Results
    domain. This maps to `many` rows in the ``studies`` table. See
    `Result_Outcome_Analysis
    <https://prsinfo.clinicaltrials.gov/results_definitions.html>`_ for
    documentation.
    """
    __tablename__ = "outcome_analyses"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    outcome_id = Column(
        Integer, ForeignKey("outcomes.id"), index=True, unique=False,
        nullable=False
    )
    non_inferiority_type = Column(
        String(60), index=False, unique=False, nullable=False
    )
    non_inferiority_description = Column(
        String(560), index=False, unique=False, nullable=True
    )
    param_type = Column(
        String(60), index=False, unique=False, nullable=True
    )
    param_value = Column(
        Float, index=False, unique=False, nullable=True
    )
    dispersion_type = Column(
        String(40), index=False, unique=False, nullable=True
    )
    dispersion_value = Column(
        Float, index=False, unique=False, nullable=True
    )
    p_value_modifier = Column(
        String(20), index=False, unique=False, nullable=True,
        doc="The p-value modifier: typically a less than or greater than sign."
    )
    p_value = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Just the numeric part of the value provided - to 6-decimal place"
        " precision."
    )
    ci_n_sides = Column(
        String(20), index=False, unique=False, nullable=True
    )
    ci_percent = Column(
        Float, index=False, unique=False, nullable=True
    )
    ci_lower_limit = Column(
        Float, index=False, unique=False, nullable=True
    )
    ci_upper_limit = Column(
        Float, index=False, unique=False, nullable=True
    )
    ci_upper_limit_na_comment = Column(
        String(380), index=False, unique=False, nullable=True
    )
    p_value_description = Column(
        String(280), index=False, unique=False, nullable=True
    )
    method = Column(
        String(60), index=False, unique=False, nullable=True
    )
    method_description = Column(
        String(180), index=False, unique=False, nullable=True
    )
    estimate_description = Column(
        String(280), index=False, unique=False, nullable=True
    )
    groups_description = Column(
        String(560), index=False, unique=False, nullable=True
    )
    other_analysis_description = Column(
        String(1100), index=False, unique=False, nullable=True
    )
    ci_upper_limit_raw = Column(
        String(255), index=False, unique=False, nullable=True
    )
    ci_lower_limit_raw = Column(
        String(255), index=False, unique=False, nullable=True
    )
    p_value_raw = Column(
        String(255), index=False, unique=False, nullable=True
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="outcome_analyses",
        doc="Relationship back to ``studies``"
    )
    outcomes = relationship(
        "Outcome", back_populates="outcome_analyses",
        doc="Relationship back to ``outcomes``"
    )
    outcome_analysis_groups = relationship(
        "OutcomeAnalysisGroup", back_populates="outcome_analyses",
        doc="Relationship back to ``outcome_analysis_groups``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class OutcomeAnalysisGroup(Base):
    """A representation of the ``outcome_analysis_groups`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    outcome_analysis_id : `int`
        The ``outcome_analysis_id`` column. Foreign key to
        ``outcome_analyses.id``.
    result_group_id : `int`
        The ``result_group_id`` column. Foreign key to ``result_groups.id``.
    ctgov_group_code : `str`
        The ``ctgov_group_code`` column (length 20).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.
    outcome_analyses : `clinical_trials.orm.OutcomeAnalysis`, optional, \
    default: `NoneType`
        Relationship with ``outcome_analyses``.
    result_groups : `clinical_trials.orm.ResultGroup`, optional, default: \
    `NoneType`
        Relationship with ``result_groups``.

    Notes
    -----
    Identifies the comparison groups that were involved with each outcome
    analysis. This is part of the ``ctgov`` schema. This is part of the Results
    domain. This maps to `many` rows in the ``studies`` table. See
    `GroupSelection
    <http://prsinfo.clinicaltrials.gov/results_definitions.html>`_ for
    documentation.
    """
    __tablename__ = "outcome_analysis_groups"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    outcome_analysis_id = Column(
        Integer, ForeignKey("outcome_analyses.id"), index=True, unique=False,
        nullable=False
    )
    result_group_id = Column(
        Integer, ForeignKey("result_groups.id"), index=True, unique=False,
        nullable=False
    )
    ctgov_group_code = Column(
        String(20), index=False, unique=False, nullable=False
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="outcome_analysis_groups",
        doc="Relationship back to ``studies``"
    )
    outcome_analyses = relationship(
        "OutcomeAnalysis", back_populates="outcome_analysis_groups",
        doc="Relationship back to ``outcome_analyses``"
    )
    result_groups = relationship(
        "ResultGroup", back_populates="outcome_analysis_groups",
        doc="Relationship back to ``result_groups``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class OutcomeCount(Base):
    """A representation of the ``outcome_counts`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    outcome_id : `int`
        The ``outcome_id`` column. Foreign key to ``outcomes.id``.
    result_group_id : `int`
        The ``result_group_id`` column. Foreign key to ``result_groups.id``.
    ctgov_group_code : `str`
        The ``ctgov_group_code`` column (length 20).
    scope : `str`
        The ``scope`` column (length 20).
    units : `str`
        The ``units`` column (length 60).
    count : `int`, optional, default: `NoneType`
        The ``count`` column.
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.
    outcomes : `clinical_trials.orm.Outcome`, optional, default: `NoneType`
        Relationship with ``outcomes``.
    result_groups : `clinical_trials.orm.ResultGroup`, optional, default: \
    `NoneType`
        Relationship with ``result_groups``.

    Notes
    -----
    Sample size included in analysis for each outcome for each study group;
    usually participants but can represent other units of measure such as eyes
    'lesions', etc. This is part of the ``ctgov`` schema. This is part of the
    Results domain. This maps to `many` rows in the ``studies`` table. See
    `OutcomeData
    <https://prsinfo.clinicaltrials.gov/results_definitions.html>`_ for
    documentation.
    """
    __tablename__ = "outcome_counts"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    outcome_id = Column(
        Integer, ForeignKey("outcomes.id"), index=True, unique=False,
        nullable=False
    )
    result_group_id = Column(
        Integer, ForeignKey("result_groups.id"), index=True, unique=False,
        nullable=False
    )
    ctgov_group_code = Column(
        String(20), index=False, unique=False, nullable=False
    )
    scope = Column(
        String(20), index=False, unique=False, nullable=False
    )
    units = Column(
        String(60), index=False, unique=False, nullable=False
    )
    count = Column(
        Integer, index=False, unique=False, nullable=True
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="outcome_counts",
        doc="Relationship back to ``studies``"
    )
    outcomes = relationship(
        "Outcome", back_populates="outcome_counts",
        doc="Relationship back to ``outcomes``"
    )
    result_groups = relationship(
        "ResultGroup", back_populates="outcome_counts",
        doc="Relationship back to ``result_groups``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class OutcomeMeasurement(Base):
    """A representation of the ``outcome_measurements`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    outcome_id : `int`
        The ``outcome_id`` column. Foreign key to ``outcomes.id``.
    result_group_id : `int`, optional, default: `NoneType`
        The ``result_group_id`` column. Foreign key to ``result_groups.id``.
    ctgov_group_code : `str`, optional, default: `NoneType`
        The ``ctgov_group_code`` column (length 20).
    classification : `str`, optional, default: `NoneType`
        The ``classification`` column (length 120).
    category : `str`, optional, default: `NoneType`
        The ``category`` column (length 120).
    title : `str`
        A concise name for the specific measure that will be used to determine
        the effect of the intervention(s) or, for observational studies (length
        300).
    description : `str`, optional, default: `NoneType`
        The ``description`` column (length 1100).
    units : `str`, optional, default: `NoneType`
        The ``units`` column (length 60).
    param_type : `str`, optional, default: `NoneType`
        The ``param_type`` column (length 40).
    param_value : `str`, optional, default: `NoneType`
        Required if the outcome measure is continuous (e.g. blood pressure)
        (length 20).
    param_value_num : `float`, optional, default: `NoneType`
        Created as a varchar dataype in order to store possible non-numeric
        values.
    dispersion_type : `str`, optional, default: `NoneType`
        The ``dispersion_type`` column (length 40).
    dispersion_value : `str`, optional, default: `NoneType`
        The ``dispersion_value`` column (length 20).
    dispersion_value_num : `float`, optional, default: `NoneType`
        The ``dispersion_value_num`` column.
    dispersion_lower_limit : `float`, optional, default: `NoneType`
        Used for reporting the lower limit of the interquartile range or full
        range.
    dispersion_upper_limit : `float`, optional, default: `NoneType`
        Used for reporting the upper limit of the interquartile range or full
        range.
    explanation_of_na : `str`, optional, default: `NoneType`
        The ``explanation_of_na`` column (length 560).
    dispersion_upper_limit_raw : `str`, optional, default: `NoneType`
        The ``dispersion_upper_limit_raw`` column (length 255).
    dispersion_lower_limit_raw : `str`, optional, default: `NoneType`
        The ``dispersion_lower_limit_raw`` column (length 255).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.
    outcomes : `clinical_trials.orm.Outcome`, optional, default: `NoneType`
        Relationship with ``outcomes``.
    result_groups : `clinical_trials.orm.ResultGroup`, optional, default: \
    `NoneType`
        Relationship with ``result_groups``.

    Notes
    -----
    Summary data for primary and secondary outcome measures for each study
    group. Includes parameter estimates and measures of dispersion/precision.
    This is part of the ``ctgov`` schema. This is part of the Results domain.
    This maps to `many` rows in the ``studies`` table. See
    `Result_Outcome_MeasureLabel
    <https://prsinfo.clinicaltrials.gov/results_definitions.html>`_ for
    documentation.
    """
    __tablename__ = "outcome_measurements"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    outcome_id = Column(
        Integer, ForeignKey("outcomes.id"), index=True, unique=False,
        nullable=False
    )
    result_group_id = Column(
        Integer, ForeignKey("result_groups.id"), index=True, unique=False,
        nullable=True
    )
    ctgov_group_code = Column(
        String(20), index=False, unique=False, nullable=True
    )
    classification = Column(
        String(120), index=False, unique=False, nullable=True
    )
    category = Column(
        String(120), index=False, unique=False, nullable=True
    )
    title = Column(
        String(300), index=False, unique=False, nullable=False,
        doc="A concise name for the specific measure that will be used to "
        "determine the effect of the intervention(s) or, for observational "
        "studies."
    )
    description = Column(
        String(1100), index=False, unique=False, nullable=True
    )
    units = Column(
        String(60), index=False, unique=False, nullable=True
    )
    param_type = Column(
        String(40), index=False, unique=False, nullable=True
    )
    param_value = Column(
        String(20), index=False, unique=False, nullable=True,
        doc="Required if the outcome measure is continuous (e.g. blood "
        "pressure)."
    )
    param_value_num = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Created as a varchar dataype in order to store possible non-"
        "numeric values."
    )
    dispersion_type = Column(
        String(40), index=False, unique=False, nullable=True
    )
    dispersion_value = Column(
        String(20), index=False, unique=False, nullable=True
    )
    dispersion_value_num = Column(
        Float, index=False, unique=False, nullable=True
    )
    dispersion_lower_limit = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Used for reporting the lower limit of the interquartile range or"
        " full range."
    )
    dispersion_upper_limit = Column(
        Float, index=False, unique=False, nullable=True,
        doc="Used for reporting the upper limit of the interquartile range or"
        " full range."
    )
    explanation_of_na = Column(
        String(560), index=False, unique=False, nullable=True
    )
    dispersion_upper_limit_raw = Column(
        String(255), index=False, unique=False, nullable=True
    )
    dispersion_lower_limit_raw = Column(
        String(255), index=False, unique=False, nullable=True
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="outcome_measurements",
        doc="Relationship back to ``studies``"
    )
    outcomes = relationship(
        "Outcome", back_populates="outcome_measurements",
        doc="Relationship back to ``outcomes``"
    )
    result_groups = relationship(
        "ResultGroup", back_populates="outcome_measurements",
        doc="Relationship back to ``result_groups``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Outcome(Base):
    """A representation of the ``outcomes`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    outcome_type : `str`
        The ``outcome_type`` column (length 40).
    title : `str`
        The ``title`` column (length 300).
    description : `str`, optional, default: `NoneType`
        The ``description`` column (length 1100).
    time_frame : `str`, optional, default: `NoneType`
        The ``time_frame`` column (length 300).
    population : `str`, optional, default: `NoneType`
        The ``population`` column (length 560).
    anticipated_posting_date : `date.date`, optional, default: `NoneType`
        This is the date-type value of anticipated_posting_month_year. The last
        day of the month is used as the 'day'.
    anticipated_posting_month_year : `str`, optional, default: `NoneType`
        A study may have records in the Outcomes table but no corresponding
        records in either the Outcome_Measures or Otucome_Counts table. These
        may be studies that have indicated which outcome measures they will
        provide results for, but have not yet posted the final results for
        these measures. For these studies and outcome measures the value of
        anticipated_posting_date indicates the anticipated date when results
        will be provided (length 20).
    units : `str`, optional, default: `NoneType`
        The ``units`` column (length 60).
    units_analyzed : `str`, optional, default: `NoneType`
        The ``units_analyzed`` column (length 60).
    dispersion_type : `str`, optional, default: `NoneType`
        The ``dispersion_type`` column (length 40).
    param_type : `str`, optional, default: `NoneType`
        The ``param_type`` column (length 40).
    outcome_analyses : `clinical_trials.orm.OutcomeAnalysis`, optional, \
    default: `NoneType`
        Relationship with ``outcome_analyses``.
    outcome_counts : `clinical_trials.orm.OutcomeCount`, optional, default: \
    `NoneType`
        Relationship with ``outcome_counts``.
    outcome_measurements : `clinical_trials.orm.OutcomeMeasurement`, \
    optional, default: `NoneType`
        Relationship with ``outcome_measurements``.
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    Descriptions of outcomes, or observation that were measured to determine
    patterns of diseases or traits, or associations with exposures, risk
    factors, or treatment. Includes information such as time frame, population
    and units. (Specific measurement results are stored in the
    Outcome_Measurements table.). This is part of the ``ctgov`` schema. This is
    part of the Results domain. This maps to `many` rows in the ``studies``
    table. See `Result_Outcome_MeasureImg
    <https://prsinfo.clinicaltrials.gov/results_definitions.html>`_ for
    documentation.
    """
    __tablename__ = "outcomes"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    outcome_type = Column(
        String(40), index=False, unique=False, nullable=False
    )
    title = Column(
        String(300), index=False, unique=False, nullable=False
    )
    description = Column(
        String(1100), index=False, unique=False, nullable=True
    )
    time_frame = Column(
        String(300), index=False, unique=False, nullable=True
    )
    population = Column(
        String(560), index=False, unique=False, nullable=True
    )
    anticipated_posting_date = Column(
        Date, index=False, unique=False, nullable=True,
        doc="This is the date-type value of anticipated_posting_month_year. "
        "The last day of the month is used as the 'day'."
    )
    anticipated_posting_month_year = Column(
        String(20), index=False, unique=False, nullable=True,
        doc="A study may have records in the Outcomes table but no "
        "corresponding records in either the Outcome_Measures or "
        "Otucome_Counts table. These may be studies that have indicated which "
        "outcome measures they will provide results for, but have not yet "
        "posted the final results for these measures. For these studies and "
        "outcome measures the value of anticipated_posting_date indicates the "
        "anticipated date when results will be provided."
    )
    units = Column(
        String(60), index=False, unique=False, nullable=True
    )
    units_analyzed = Column(
        String(60), index=False, unique=False, nullable=True
    )
    dispersion_type = Column(
        String(40), index=False, unique=False, nullable=True
    )
    param_type = Column(
        String(40), index=False, unique=False, nullable=True
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    outcome_analyses = relationship(
        "OutcomeAnalysis", back_populates="outcomes",
        doc="Relationship back to ``outcome_analyses``."
    )
    outcome_counts = relationship(
        "OutcomeCount", back_populates="outcomes",
        doc="Relationship back to ``outcome_counts``."
    )
    outcome_measurements = relationship(
        "OutcomeMeasurement", back_populates="outcomes",
        doc="Relationship back to ``outcome_measurements``."
    )
    studies = relationship(
        "Study", back_populates="outcomes",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class OverallOfficial(Base):
    """A representation of the ``overall_officials`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    role : `str`, optional, default: `NoneType`
        The ``role`` column (length 40).
    name : `str`
        The ``name`` column (length 160).
    affiliation : `str`, optional, default: `NoneType`
        The ``affiliation`` column (length 300).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    People responsible for the overall scientific leadership of the protocol
    including the principal investigator. This is part of the ``ctgov`` schema.
    This is part of the Protocol domain. This maps to `many` rows in the
    ``studies`` table. See `StudyOfficials
    <https://prsinfo.clinicaltrials.gov/definitions.html>`_ for documentation.
    """
    __tablename__ = "overall_officials"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    role = Column(
        String(40), index=False, unique=False, nullable=True
    )
    name = Column(
        String(160), index=False, unique=False, nullable=False
    )
    affiliation = Column(
        String(300), index=False, unique=False, nullable=True
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="overall_officials",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ParticipantFlow(Base):
    """A representation of the ``participant_flows`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    recruitment_details : `str`, optional, default: `NoneType`
        The ``recruitment_details`` column (length 560).
    pre_assignment_details : `str`, optional, default: `NoneType`
        The ``pre_assignment_details`` column (length 560).
    units_analyzed : `str`, optional, default: `NoneType`
        The ``units_analyzed`` column (length 60).
    drop_withdraw_comment : `str`, optional, default: `NoneType`
        The ``drop_withdraw_comment`` column (length 255).
    reason_comment : `str`, optional, default: `NoneType`
        The ``reason_comment`` column (length 255).
    count_units : `str`, optional, default: `NoneType`
        The ``count_units`` column (length 255).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    Recruitment information relevant to the recruitment process & pre-
    assignment details (ie. significant events in the study that occur after
    participant enrollment, but prior to assignment of participants).
    Information about participant flow that applies to all milestones. This is
    part of the ``ctgov`` schema. This is part of the Results domain. This maps
    to `one` row in the ``studies`` table. See `Result_ParticipantFlowLabel
    <https://prsinfo.clinicaltrials.gov/results_definitions.html>`_ for
    documentation.
    """
    __tablename__ = "participant_flows"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    recruitment_details = Column(
        String(560), index=False, unique=False, nullable=True
    )
    pre_assignment_details = Column(
        String(560), index=False, unique=False, nullable=True
    )
    units_analyzed = Column(
        String(60), index=False, unique=False, nullable=True
    )
    drop_withdraw_comment = Column(
        String(255), index=False, unique=False, nullable=True
    )
    reason_comment = Column(
        String(255), index=False, unique=False, nullable=True
    )
    count_units = Column(
        String(255), index=False, unique=False, nullable=True
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="participant_flows",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PendingResult(Base):
    """A representation of the ``pending_results`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    event : `str`
        Uses the tag label to identify the type of event this row represents
        (length 20).
    event_date_description : `str`
        The value might be 'Unknown', so a date description (string-type)
        column is provided to capture this (length 20).
    event_date : `date.date`, optional, default: `NoneType`
        Convert the value to a date-type.
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    Provides information about events related to the submission of study
    results for quality control (QC) review before the results are publicly
    posted. Events reported: submissions, cancellations and returns for
    modifications. 'Unknown' is specified for cancellations that occurred
    before 05/08/2018 (when this data began being collected). When a study
    passes quality control review: 1) results_first_submitted_date is set to
    the study's first submission date, 2) results_first_submitted_qc_date is
    set to the submission date of the version of results that passed QC, 3) the
    study's pending_results rows are removed, and 4) the results are posted on
    ClinicalTrials.gov. The latest versions of all studies are posted every
    business day but, there can be unexpected delays. The
    results_first_posted_date value will usually be identified as an 'Estimate'
    when first posted. This will switch to 'Actual' (and the date may be
    adjusted) on the next posting cycle, when the true posting date is known.
    This is part of the ``ctgov`` schema. This is part of the Results domain.
    This maps to `many` rows in the ``studies`` table.
    """
    __tablename__ = "pending_results"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    event = Column(
        String(20), index=False, unique=False, nullable=False,
        doc="Uses the tag label to identify the type of event this row "
        "represents."
    )
    event_date_description = Column(
        String(20), index=False, unique=False, nullable=False,
        doc="The value might be 'Unknown', so a date description (string-"
        "type) column is provided to capture this."
    )
    event_date = Column(
        Date, index=False, unique=False, nullable=True,
        doc="Convert the value to a date-type."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="pending_results",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ProvidedDocument(Base):
    """A representation of the ``provided_documents`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    document_type : `str`
        The ``document_type`` column (length 140).
    has_protocol : `bool`
        The ``has_protocol`` column.
    has_icf : `bool`
        The ``has_icf`` column.
    has_sap : `bool`
        The ``has_sap`` column.
    document_date : `date.date`
        The ``document_date`` column.
    url : `str`
        The ``url`` column (length 100).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    The full study protocol and statistical analysis plan must be uploaded as
    part of results information submission, for studies with a Primary
    Completion Date on or after January 18, 2017. The protocol and statistical
    analysis plan may be optionally uploaded before results information
    submission and updated with new versions, as needed. Informed consent forms
    may optionally be uploaded at any time. This is part of the ``ctgov``
    schema. This is part of the Results domain. This maps to `many` rows in the
    ``studies`` table. See `Documents
    <https://prsinfo.clinicaltrials.gov/results_definitions.html>`_ for
    documentation.
    """
    __tablename__ = "provided_documents"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    document_type = Column(
        String(140), index=False, unique=False, nullable=False
    )
    has_protocol = Column(
        Boolean, index=False, unique=False, nullable=False
    )
    has_icf = Column(
        Boolean, index=False, unique=False, nullable=False
    )
    has_sap = Column(
        Boolean, index=False, unique=False, nullable=False
    )
    document_date = Column(
        Date, index=False, unique=False, nullable=False
    )
    url = Column(
        String(100), index=False, unique=False, nullable=False
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="provided_documents",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ReportedEvent(Base):
    """A representation of the ``reported_events`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    result_group_id : `int`
        The ``result_group_id`` column. Foreign key to ``result_groups.id``.
    ctgov_group_code : `str`
        The ``ctgov_group_code`` column (length 20).
    time_frame : `str`, optional, default: `NoneType`
        The ``time_frame`` column (length 560).
    event_type : `str`
        serious' if data from <serious_events> 'other' if data from
        <other_events> (length 20).
    default_vocab : `str`, optional, default: `NoneType`
        The ``default_vocab`` column (length 255).
    default_assessment : `str`, optional, default: `NoneType`
        The ``default_assessment`` column (length 255).
    subjects_affected : `int`, optional, default: `NoneType`
        The ``subjects_affected`` column.
    subjects_at_risk : `int`
        The ``subjects_at_risk`` column.
    description : `str`, optional, default: `NoneType`
        The ``description`` column (length 560).
    event_count : `int`, optional, default: `NoneType`
        The ``event_count`` column.
    organ_system : `str`
        The ``organ_system`` column (length 80).
    adverse_event_term : `str`, optional, default: `NoneType`
        The ``adverse_event_term`` column (length 120).
    frequency_threshold : `int`, optional, default: `NoneType`
        The ``frequency_threshold`` column.
    vocab : `str`, optional, default: `NoneType`
        The ``vocab`` column (length 40).
    assessment : `str`, optional, default: `NoneType`
        The ``assessment`` column (length 40).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.
    result_groups : `clinical_trials.orm.ResultGroup`, optional, default: \
    `NoneType`
        Relationship with ``result_groups``.

    Notes
    -----
    Summary information about reported adverse events (any untoward or
    unfavorable medical occurrence to participants, including abnormal physical
    exams, laboratory findings, symptoms, or diseases), including serious
    adverse events, other adverse events, and mortality. This is part of the
    ``ctgov`` schema. This is part of the Results domain. This maps to `many`
    rows in the ``studies`` table. See `Result_AdverseEventsLabel
    <https://prsinfo.clinicaltrials.gov/results_definitions.html>`_ for
    documentation.
    """
    __tablename__ = "reported_events"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    result_group_id = Column(
        Integer, ForeignKey("result_groups.id"), index=True, unique=False,
        nullable=False
    )
    ctgov_group_code = Column(
        String(20), index=False, unique=False, nullable=False
    )
    time_frame = Column(
        String(560), index=False, unique=False, nullable=True
    )
    event_type = Column(
        String(20), index=False, unique=False, nullable=False,
        doc="serious' if data from <serious_events> 'other' if data from "
        "<other_events>."
    )
    default_vocab = Column(
        String(255), index=False, unique=False, nullable=True
    )
    default_assessment = Column(
        String(255), index=False, unique=False, nullable=True
    )
    subjects_affected = Column(
        Integer, index=False, unique=False, nullable=True
    )
    subjects_at_risk = Column(
        Integer, index=False, unique=False, nullable=False
    )
    description = Column(
        String(560), index=False, unique=False, nullable=True
    )
    event_count = Column(
        Integer, index=False, unique=False, nullable=True
    )
    organ_system = Column(
        String(80), index=False, unique=False, nullable=False
    )
    adverse_event_term = Column(
        String(120), index=False, unique=False, nullable=True
    )
    frequency_threshold = Column(
        Integer, index=False, unique=False, nullable=True
    )
    vocab = Column(
        String(40), index=False, unique=False, nullable=True
    )
    assessment = Column(
        String(40), index=False, unique=False, nullable=True
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="reported_events",
        doc="Relationship back to ``studies``"
    )
    result_groups = relationship(
        "ResultGroup", back_populates="reported_events",
        doc="Relationship back to ``result_groups``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ReportedEventTotal(Base):
    """A representation of the ``reported_event_totals`` table.

    Parameters
    ----------
    id : `int`
        The AACT primary key column.
    nct_id : `str`
        The clinicaltrials.gov study identifier. Foreign key to
        ``studies.nct_id`` (length 20).
    ctgov_group_code : `str`
        The clinicaltrials.gov result group code (length 20).
    event_type : `str`
        The event type, i.e. serious or death (length 20).
    classification : `str`
        The event type classification (length 40).
    subjects_affected : `int`, optional, default: `NoneType`
        The number of subjects in the study that had the event.
    subjects_at_risk : `int`, optional, default: `NoneType`
        The number of subjects in the study at risk of the event.
    created_at : `str`
        The datetime the row was created (length 40).
    updated_at : `str`
        The datetime the row was updated (length 40).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.
    """
    __tablename__ = "reported_event_totals"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="The AACT primary key column."
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False, doc="The clinicaltrials.gov study identifier."
    )
    ctgov_group_code = Column(
        String(20), index=False, unique=False, nullable=False,
        doc="The clinicaltrials.gov result group code."
    )
    event_type = Column(
        String(20), index=False, unique=False, nullable=False,
        doc="The event type, i.e. serious or death."
    )
    classification = Column(
        String(40), index=False, unique=False, nullable=False,
        doc="The event type classification."
    )
    subjects_affected = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="The number of subjects in the study that had the event."
    )
    subjects_at_risk = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="The number of subjects in the study at risk of the event."
    )
    created_at = Column(
        String(40), index=False, unique=False, nullable=False,
        doc="The datetime the row was created."
    )
    updated_at = Column(
        String(40), index=False, unique=False, nullable=False,
        doc="The datetime the row was updated."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="reported_event_totals",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ResponsibleParty(Base):
    """A representation of the ``responsible_parties`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    responsible_party_type : `str`, optional, default: `NoneType`
        This data element was introduced by ClinicalTrials.gov in August 2011
        (length 40).
    name : `str`, optional, default: `NoneType`
        The ``name`` column (length 80).
    title : `str`, optional, default: `NoneType`
        This data element was introduced by ClinicalTrials.gov in August 2011
        and is required if either Principal Investigator or Sponsor-
        Investigator is selected for the responsible_party_type (length 280).
    organization : `str`, optional, default: `NoneType`
        This data element was introduced in November 2007 and discontinued in
        August 2011 (length 240).
    affiliation : `str`, optional, default: `NoneType`
        The ``affiliation`` column (length 140).
    old_name_title : `str`, optional, default: `NoneType`
        The ``old_name_title`` column (length 240).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    People who have access to and control over the data from the study, have
    the right to publish study results, and have the ability to meet all of the
    requirements for the submission of study information. This is part of the
    ``ctgov`` schema. This is part of the Protocol domain. This maps to `many`
    rows in the ``studies`` table. See `RespParty
    <https://prsinfo.clinicaltrials.gov/definitions.html>`_ for documentation.
    """
    __tablename__ = "responsible_parties"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    responsible_party_type = Column(
        String(40), index=False, unique=False, nullable=True,
        doc="This data element was introduced by ClinicalTrials.gov in August"
        " 2011."
    )
    name = Column(
        String(80), index=False, unique=False, nullable=True
    )
    title = Column(
        String(280), index=False, unique=False, nullable=True,
        doc="This data element was introduced by ClinicalTrials.gov in August"
        " 2011 and is required if either Principal Investigator or Sponsor-"
        "Investigator is selected for the responsible_party_type."
    )
    organization = Column(
        String(240), index=False, unique=False, nullable=True,
        doc="This data element was introduced in November 2007 and "
        "discontinued in August 2011."
    )
    affiliation = Column(
        String(140), index=False, unique=False, nullable=True
    )
    old_name_title = Column(
        String(240), index=False, unique=False, nullable=True
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="responsible_parties",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ResultAgreement(Base):
    """A representation of the ``result_agreements`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    pi_employee : `bool`
        NLM data dictionary reports this as Yes/No, but data actually stored as
        an affirmative statement or a negative statement as indicated in the
        enumerations tab. Studies with a negative value for this variable
        provide information about any results disclosure restrictions on PI(s).
        This information is captured in the RESTRICTIVE_AGREEMENT variable in
        the RESULTS_RESTRICTION_AGREEMENTS table.
    agreement : `str`, optional, default: `NoneType`
        If the user answers “N” to the question about Results Disclosure
        Restriction on PI(s), or negatively whether all PI(s) are employees of
        the sponsor, the field contains the statement, 'There is NOT an
        agreement between the sponsor (or its agents) that restricts the PI's
        rights to discuss or publish trial results after the trial is
        completed.' (length 255).
    restriction_type : `str`, optional, default: `NoneType`
        The ``restriction_type`` column (length 20).
    other_details : `str`, optional, default: `NoneType`
        The ``other_details`` column (length 560).
    restrictive_agreement : `str`, optional, default: `NoneType`
        The ``restrictive_agreement`` column (length 560).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    Info about whether an agreement exists between the sponsor & the principal
    investigators (PIs) that restricts the PIs ability to discuss study results
    at scientific meetings or other public or private forums, or to publish
    info concerning the study in scientific or academic journals after the
    study is completed. This is part of the ``ctgov`` schema. This is part of
    the Results domain. This maps to `many` rows in the ``studies`` table. See
    `Result_CertainAgreementLabel
    <https://prsinfo.clinicaltrials.gov/results_definitions.html>`_ for
    documentation.
    """
    __tablename__ = "result_agreements"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    pi_employee = Column(
        Boolean, index=False, unique=False, nullable=False,
        doc="NLM data dictionary reports this as Yes/No, but data actually "
        "stored as an affirmative statement or a negative statement as "
        "indicated in the enumerations tab. Studies with a negative value for "
        "this variable provide information about any results disclosure "
        "restrictions on PI(s). This information is captured in the "
        "RESTRICTIVE_AGREEMENT variable in the RESULTS_RESTRICTION_AGREEMENTS "
        "table."
    )
    agreement = Column(
        String(255), index=False, unique=False, nullable=True,
        doc="If the user answers “N” to the question about Results Disclosure"
        " Restriction on PI(s), or negatively whether all PI(s) are employees "
        "of the sponsor, the field contains the statement, 'There is NOT an "
        "agreement between the sponsor (or its agents) that restricts the PI's"
        " rights to discuss or publish trial results after the trial is "
        "completed.'."
    )
    restriction_type = Column(
        String(20), index=False, unique=False, nullable=True
    )
    other_details = Column(
        String(560), index=False, unique=False, nullable=True
    )
    restrictive_agreement = Column(
        String(560), index=False, unique=False, nullable=True
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="result_agreements",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ResultContact(Base):
    """A representation of the ``result_contacts`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    organization : `str`
        The ``organization`` column (length 240).
    name : `str`
        The ``name`` column (length 120).
    phone : `str`, optional, default: `NoneType`
        The ``phone`` column (length 40).
    email : `str`, optional, default: `NoneType`
        The ``email`` column (length 100).
    extension : `str`, optional, default: `NoneType`
        The ``extension`` column (length 20).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    Point of contact for scientific information about the clinical study
    results information. This is part of the ``ctgov`` schema. This is part of
    the Results domain. This maps to `many` rows in the ``studies`` table. See
    `Result_PointOfContactLabel
    <https://prsinfo.clinicaltrials.gov/results_definitions.html>`_ for
    documentation.
    """
    __tablename__ = "result_contacts"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    organization = Column(
        String(240), index=False, unique=False, nullable=False
    )
    name = Column(
        String(120), index=False, unique=False, nullable=False
    )
    phone = Column(
        String(40), index=False, unique=False, nullable=True
    )
    email = Column(
        String(100), index=False, unique=False, nullable=True
    )
    extension = Column(
        String(20), index=False, unique=False, nullable=True
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="result_contacts",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ResultGroup(Base):
    """A representation of the ``result_groups`` table.

    Parameters
    ----------
    id : `int`
        This table is an aggregate colleciton of group info gathered from
        baseline_measures, reported_events, outcomes, milestones, and
        drop_withdrawals.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    ctgov_group_code : `str`
        The ``ctgov_group_code`` column (length 20).
    result_type : `str`
        Derived from XML tag: 'Baseline', 'Outcome', 'Reported Event', or
        'Participant Flow' (length 20).
    title : `str`, optional, default: `NoneType`
        The ``title`` column (length 120).
    description : `str`, optional, default: `NoneType`
        The ``description`` column (length 1660).
    baseline_counts : `clinical_trials.orm.BaselineCount`, optional, default: \
    `NoneType`
        Relationship with ``baseline_counts``.
    baseline_measurements : `clinical_trials.orm.BaselineMeasurement`, \
    optional, default: `NoneType`
        Relationship with ``baseline_measurements``.
    drop_withdrawals : `clinical_trials.orm.DropWithdrawal`, optional, \
    default: `NoneType`
        Relationship with ``drop_withdrawals``.
    milestones : `clinical_trials.orm.Milestone`, optional, default: \
    `NoneType`
        Relationship with ``milestones``.
    outcome_analysis_groups : `clinical_trials.orm.OutcomeAnalysisGroup`, \
    optional, default: `NoneType`
        Relationship with ``outcome_analysis_groups``.
    outcome_counts : `clinical_trials.orm.OutcomeCount`, optional, default: \
    `NoneType`
        Relationship with ``outcome_counts``.
    outcome_measurements : `clinical_trials.orm.OutcomeMeasurement`, \
    optional, default: `NoneType`
        Relationship with ``outcome_measurements``.
    reported_events : `clinical_trials.orm.ReportedEvent`, optional, default: \
    `NoneType`
        Relationship with ``reported_events``.
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    Consolidated, aggregate list of group titles/descriptions used for
    reporting summary results information. This is part of the ``ctgov``
    schema. This is part of the Results domain. This maps to `many` rows in the
    ``studies`` table. See `PopFlowArmGroup
    <https://prsinfo.clinicaltrials.gov/results_definitions.html>`_ for
    documentation.
    """
    __tablename__ = "result_groups"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="This table is an aggregate colleciton of group info gathered "
        "from baseline_measures, reported_events, outcomes, milestones, and "
        "drop_withdrawals."
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    ctgov_group_code = Column(
        String(20), index=False, unique=False, nullable=False
    )
    result_type = Column(
        String(20), index=False, unique=False, nullable=False,
        doc="Derived from XML tag: 'Baseline', 'Outcome', 'Reported Event', "
        "or 'Participant Flow'."
    )
    title = Column(
        String(120), index=False, unique=False, nullable=True
    )
    description = Column(
        String(1660), index=False, unique=False, nullable=True
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    baseline_counts = relationship(
        "BaselineCount", back_populates="result_groups",
        doc="Relationship back to ``baseline_counts``."
    )
    baseline_measurements = relationship(
        "BaselineMeasurement", back_populates="result_groups",
        doc="Relationship back to ``baseline_measurements``."
    )
    drop_withdrawals = relationship(
        "DropWithdrawal", back_populates="result_groups",
        doc="Relationship back to ``drop_withdrawals``."
    )
    milestones = relationship(
        "Milestone", back_populates="result_groups",
        doc="Relationship back to ``milestones``."
    )
    outcome_analysis_groups = relationship(
        "OutcomeAnalysisGroup", back_populates="result_groups",
        doc="Relationship back to ``outcome_analysis_groups``."
    )
    outcome_counts = relationship(
        "OutcomeCount", back_populates="result_groups",
        doc="Relationship back to ``outcome_counts``."
    )
    outcome_measurements = relationship(
        "OutcomeMeasurement", back_populates="result_groups",
        doc="Relationship back to ``outcome_measurements``."
    )
    reported_events = relationship(
        "ReportedEvent", back_populates="result_groups",
        doc="Relationship back to ``reported_events``."
    )
    studies = relationship(
        "Study", back_populates="result_groups",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Retraction(Base):
    """A representation of the ``retractions`` table.

    Parameters
    ----------
    id : `int`
        The AACT primary key column.
    reference_id : `int`
        The ``reference_id`` column.
    pmid : `int`
        The pubmed identifier of the retraction.
    source : `str`
        The ``source`` column (length 280).
    nct_id : `str`
        The clinicaltrials.gov study identifier. Foreign key to
        ``studies.nct_id`` (length 20).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.
    """
    __tablename__ = "retractions"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True, doc="The AACT primary key column."
    )
    reference_id = Column(
        Integer, index=False, unique=False, nullable=False
    )
    pmid = Column(
        Integer, index=False, unique=False, nullable=False,
        doc="The pubmed identifier of the retraction."
    )
    source = Column(
        String(280), index=False, unique=False, nullable=False
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False, doc="The clinicaltrials.gov study identifier."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="retractions",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SearchResult(Base):
    """A representation of the ``search_results`` table.

    Parameters
    ----------
    id : `str`
        The ``id`` column (length 255).
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 255).
    name : `str`
        short name of the study_search this object belongs to (length 255).
    created_at : `str`
        The ``created_at`` column (length 255).
    updated_at : `str`
        The ``updated_at`` column (length 255).
    grouping : `str`
        category the object is grouped under (length 255).
    study_search_id : `str`
        connects the object to the study_search that pulled it up (length 255).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    This joins studies with saved queries withing the study_searches table.
    This is part of the ``ctgov`` schema. This maps to `many` rows in the
    ``studies`` table.
    """
    __tablename__ = "search_results"

    id = Column(
        String(255), Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(255), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    name = Column(
        String(255), index=False, unique=False, nullable=False,
        doc="short name of the study_search this object belongs to."
    )
    created_at = Column(
        String(255), index=False, unique=False, nullable=False
    )
    updated_at = Column(
        String(255), index=False, unique=False, nullable=False
    )
    grouping = Column(
        String(255), index=False, unique=False, nullable=False,
        doc="category the object is grouped under."
    )
    study_search_id = Column(
        String(255), index=False, unique=False, nullable=False,
        doc="connects the object to the study_search that pulled it up."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="search_results",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Sponsor(Base):
    """A representation of the ``sponsors`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    agency_class : `str`, optional, default: `NoneType`
        NLM derived value that indicates the broad catergory of sponsor (length
        20).
    lead_or_collaborator : `str`
        lead' if value from <lead_sponsor> 'collaborator' if value from
        <collaborator> (length 20).
    name : `str`
        The ``name`` column (length 180).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    Name of study sponsors and collaborators. The sponsor is the entity or
    individual initiating the study. Collaborators are other organizations
    providing support, including funding, design, implementation, data
    analysis, and reporting. This is part of the ``ctgov`` schema. This is part
    of the Protocol domain. This maps to `many` rows in the ``studies`` table.
    See `LeadSponsor <https://prsinfo.clinicaltrials.gov/definitions.html>`_
    for documentation.
    """
    __tablename__ = "sponsors"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    agency_class = Column(
        String(20), index=False, unique=False, nullable=True,
        doc="NLM derived value that indicates the broad catergory of sponsor."
    )
    lead_or_collaborator = Column(
        String(20), index=False, unique=False, nullable=False,
        doc="lead' if value from <lead_sponsor> 'collaborator' if value from "
        "<collaborator>."
    )
    name = Column(
        String(180), index=False, unique=False, nullable=False
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="sponsors",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Study(Base):
    """A representation of the ``studies`` table.

    Parameters
    ----------
    nct_id : `str`
        NCT ID is a unique identification code given to each clinical study
        registered on ClinicalTrials.gov. The format is the letters 'NCT'
        followed by an 8-digit number (for example, NCT00000419) (length 20).
    nlm_download_date_description : `str`, optional, default: `NoneType`
        Date NLM made study available via their API (length 255).
    study_first_submitted_date : `date.date`
        The _submitted suffix indicates the date that the responsible party
        pressed the release button in the ClinicalTrials.gov data submission
        system, thereby submitting the data for review prior to posting.
    results_first_submitted_date : `date.date`, optional, default: `NoneType`
        This value is only available for studies that have results. It is the
        date when the results were first received. The _submitted suffix
        indicates the date that the responsible party pressed the release
        button in the ClinicalTrials.gov data submission system, thereby
        submitting the data for review prior to posting.
    disposition_first_submitted_date : `date.date`, optional, default: \
    `NoneType`
        Date when a sponsor indicates that they will be delaying posting of
        results (and are certifying initial approval or new use), or when they
        apply for an extension of the deadline. The _submitted suffix indicates
        the date that the responsible party pressed the release button in the
        ClinicalTrials.gov data submission system, thereby submitting the data
        for review prior to posting.
    last_update_submitted_date : `date.date`
        The _submitted suffix indicates the date that the responsible party
        pressed the release button in the ClinicalTrials.gov data submission
        system, thereby submitting the data for review prior to posting.
    study_first_submitted_qc_date : `date.date`
        The _submitted_qc suffix indicates the submission date of the version
        of the record that met quality control criteria. It is common for there
        to be revisions between the first submission and the submission that
        meets qc criteria. Note this is not the date that the qc review was
        completed.
    study_first_posted_date : `date.date`
        The _posted suffix indicates the date that a submission was made public
        on ClinicalTrials.gov. Typically, posting occurs the business day after
        qc review was successfully completed. We began collecting posted dates
        in early 2017. Dates from before then are estmates. All posted dates
        include a type attribute of Actual or Estimate.
    study_first_posted_date_type : `str`
        The ``study_first_posted_date_type`` column (length 20).
    results_first_submitted_qc_date : `date.date`, optional, default: \
    `NoneType`
        The _submitted_qc suffix indicates the submission date of the version
        of the record that met quality control criteria. It is common for there
        to be revisions between the first submission and the submission that
        meets qc criteria. Note this is not the date that the qc review was
        completed.
    results_first_posted_date : `date.date`, optional, default: `NoneType`
        The _posted suffix indicates the date that a submission was made public
        on ClinicalTrials.gov. Typically, posting occurs the business day after
        qc review was successfully completed. We began collecting posted dates
        in early 2017. Dates from before then are estmates. All posted dates
        include a type attribute of Actual or Estimate.
    results_first_posted_date_type : `str`, optional, default: `NoneType`
        The ``results_first_posted_date_type`` column (length 20).
    disposition_first_submitted_qc_date : `date.date`, optional, default: \
    `NoneType`
        The _submitted_qc suffix indicates the submission date of the version
        of the record that met quality control criteria. It is common for there
        to be revisions between the first submission and the submission that
        meets qc criteria. Note this is not the date that the qc review was
        completed.
    disposition_first_posted_date : `date.date`, optional, default: \
    `NoneType`
        The _posted suffix indicates the date that a submission was made public
        on ClinicalTrials.gov. Typically, posting occurs the business day after
        qc review was successfully completed. We began collecting posted dates
        in early 2017. Dates from before then are estmates. All posted dates
        include a type attribute of Actual or Estimate.
    disposition_first_posted_date_type : `str`, optional, default: `NoneType`
        The ``disposition_first_posted_date_type`` column (length 20).
    last_update_submitted_qc_date : `date.date`
        The _submitted_qc suffix indicates the submission date of the version
        of the record that met quality control criteria. It is common for there
        to be revisions between the first submission and the submission that
        meets qc criteria. Note this is not the date that the qc review was
        completed.
    last_update_posted_date : `date.date`
        From a utility standpoint, the new <last_update_posted> date is likely
        the date that most people want to search. We previously did not have
        this date available and the old <lastchanged_date> was the closest
        alternative, though problematic because it never meant what was implied
        by the name. The _posted suffix indicates the date that a submission
        was made public on ClinicalTrials.gov. Typically, posting occurs the
        business day after qc review was successfully completed. We began
        collecting posted dates in early 2017. Dates from before then are
        estmates. All posted dates include a type attribute of Actual or
        Estimate.
    last_update_posted_date_type : `str`
        The ``last_update_posted_date_type`` column (length 20).
    start_month_year : `str`, optional, default: `NoneType`
        The value of the start date exactly as it is provided by
        ClinicalTrials.gov. Some studies have only month/year defined for the
        start date (ie. October, 2014). That value is stored in this column and
        the start_date column converts this value to a date by assigning it the
        first day of the month (length 20).
    start_date_type : `str`, optional, default: `NoneType`
        The ``start_date_type`` column (length 20).
    start_date : `date.date`, optional, default: `NoneType`
        The date the study started in a date-type format so that it can be used
        to find studies before/after/inclusive of a date or dates. For studies
        that only provide month/year, the last day of the month is used.
    verification_month_year : `str`, optional, default: `NoneType`
        The value of the verification date exactly as it is provided by
        ClinicalTrials.gov. Some studies have only month/year defined for the
        verification date (ie. October, 2014). That value is stored in this
        column and the verification_date column converts this value to a date
        by assigning it the first day of the month (length 20).
    verification_date : `date.date`, optional, default: `NoneType`
        The date the study was last verified in a date-type format so that it
        can be used to find studies before/after/inclusive of a date or dates.
        For studies that only provide month/year, the last day of the month is
        used.
    completion_month_year : `str`, optional, default: `NoneType`
        The value of the completion date exactly as it is provided by
        ClinicalTrials.gov. Some studies have only month/year defined for the
        completion date (ie. October, 2014). That value is stored in this
        column and the completion_date column converts this value to a date by
        assigning it the first day of the month (length 20).
    completion_date_type : `str`, optional, default: `NoneType`
        The ``completion_date_type`` column (length 20).
    completion_date : `date.date`, optional, default: `NoneType`
        The date the study was completed (as defined by NLM) in a date-type
        format so that it can be used to find studies before/after/inclusive of
        a date or dates. For studies that only provide month/year, the last day
        of the month is used.
    primary_completion_month_year : `str`, optional, default: `NoneType`
        The value of the primary completion date exactly as it is provided by
        ClinicalTrials.gov. Some studies have only month/year defined for the
        primary completion date (ie. October, 2014). That value is stored in
        this column and the primary_completion_date column converts this value
        to a date by assigning it the first day of the month (length 20).
    primary_completion_date_type : `str`, optional, default: `NoneType`
        The ``primary_completion_date_type`` column (length 20).
    primary_completion_date : `date.date`, optional, default: `NoneType`
        The primary completion date for the study (as defined by NLM) in a
        date-type format so that it can be used to find studies
        before/after/inclusive of a date or dates. For studies that only
        provide month/year, the last day of the month is used.
    target_duration : `str`, optional, default: `NoneType`
        The ``target_duration`` column (length 20).
    study_type : `str`, optional, default: `NoneType`
        The ``study_type`` column (length 40).
    acronym : `str`, optional, default: `NoneType`
        The ``acronym`` column (length 20).
    baseline_population : `str`, optional, default: `NoneType`
        The ``baseline_population`` column (length 560).
    brief_title : `str`
        The ``brief_title`` column (length 340).
    official_title : `str`, optional, default: `NoneType`
        The ``official_title`` column (length 660).
    overall_status : `str`
        The ``overall_status`` column (length 40).
    last_known_status : `str`, optional, default: `NoneType`
        The ``last_known_status`` column (length 40).
    phase : `str`, optional, default: `NoneType`
        The ``phase`` column (length 20).
    enrollment : `int`, optional, default: `NoneType`
        For Interventional Study Design, Enrollment is required by FDAAA. For
        Observational Study Design, Enrollment is required by
        ClinicalTrials.gov (*).
    enrollment_type : `str`, optional, default: `NoneType`
        The ``enrollment_type`` column (length 20).
    source : `str`
        The ``source`` column (length 180).
    limitations_and_caveats : `str`, optional, default: `NoneType`
        The ``limitations_and_caveats`` column (length 255).
    number_of_arms : `int`, optional, default: `NoneType`
        The number of groups for interventional studies, it is NULL when the
        study is not an interventional study.
    number_of_groups : `int`, optional, default: `NoneType`
        The number of arms for interventional studies, it is NULL when the
        study is an interventional study.
    why_stopped : `str`, optional, default: `NoneType`
        The ``why_stopped`` column (length 280).
    has_expanded_access : `bool`, optional, default: `NoneType`
        The ``has_expanded_access`` column.
    expanded_access_type_individual : `bool`, optional, default: `NoneType`
        The ``expanded_access_type_individual`` column.
    expanded_access_type_intermediate : `bool`, optional, default: `NoneType`
        The ``expanded_access_type_intermediate`` column.
    expanded_access_type_treatment : `bool`, optional, default: `NoneType`
        The ``expanded_access_type_treatment`` column.
    has_dmc : `bool`, optional, default: `NoneType`
        The ``has_dmc`` column.
    is_fda_regulated_drug : `bool`, optional, default: `NoneType`
        The ``is_fda_regulated_drug`` column.
    is_fda_regulated_device : `bool`, optional, default: `NoneType`
        The ``is_fda_regulated_device`` column.
    is_unapproved_device : `bool`, optional, default: `NoneType`
        The ``is_unapproved_device`` column.
    is_ppsd : `bool`, optional, default: `NoneType`
        The ``is_ppsd`` column.
    is_us_export : `bool`, optional, default: `NoneType`
        Product Manufactured in and Exported from the U.S. Definition: Whether
        any drug product (including a biological product) or device product
        studied in the clinical study is manufactured in the United States or
        one of its territories and exported for study in a clinical study in
        another country. Select Yes/No.
    biospec_retention : `str`, optional, default: `NoneType`
        The ``biospec_retention`` column (length 40).
    biospec_description : `str`, optional, default: `NoneType`
        The ``biospec_description`` column (length 1100).
    ipd_time_frame : `str`, optional, default: `NoneType`
        The ``ipd_time_frame`` column (length 1100).
    ipd_access_criteria : `str`, optional, default: `NoneType`
        The ``ipd_access_criteria`` column (length 1100).
    ipd_url : `str`, optional, default: `NoneType`
        The ``ipd_url`` column (length 220).
    plan_to_share_ipd : `str`, optional, default: `NoneType`
        The ``plan_to_share_ipd`` column (length 20).
    plan_to_share_ipd_description : `str`, optional, default: `NoneType`
        The ``plan_to_share_ipd_description`` column (length 1100).
    created_at : `str`
        (AACT assigned timestamp) Date/time the study was loaded into AACT from
        ClinicalTrials.gov (length 40).
    updated_at : `str`
        AACT assigned timestamp) Last date/time the row was modified (At this
        point, this value should always be same ascreated_at (length 40).
    source_class : `str`, optional, default: `NoneType`
        The ``source_class`` column (length 20).
    delayed_posting : `bool`, optional, default: `NoneType`
        The ``delayed_posting`` column.
    expanded_access_nctid : `str`, optional, default: `NoneType`
        The ``expanded_access_nctid`` column (length 20).
    expanded_access_status_for_nctid : `str`, optional, default: `NoneType`
        The ``expanded_access_status_for_nctid`` column (length 40).
    fdaaa801_violation : `bool`, optional, default: `NoneType`
        The ``fdaaa801_violation`` column.
    baseline_type_units_analyzed : `str`, optional, default: `NoneType`
        The ``baseline_type_units_analyzed`` column (length 60).
    baseline_counts : `clinical_trials.orm.BaselineCount`, optional, default: \
    `NoneType`
        Relationship with ``baseline_counts``.
    baseline_measurements : `clinical_trials.orm.BaselineMeasurement`, \
    optional, default: `NoneType`
        Relationship with ``baseline_measurements``.
    brief_summaries : `clinical_trials.orm.BriefSummary`, optional, default: \
    `NoneType`
        Relationship with ``brief_summaries``.
    browse_conditions : `clinical_trials.orm.BrowseCondition`, optional, \
    default: `NoneType`
        Relationship with ``browse_conditions``.
    browse_interventions : `clinical_trials.orm.BrowseIntervention`, \
    optional, default: `NoneType`
        Relationship with ``browse_interventions``.
    calculated_values : `clinical_trials.orm.CalculatedValue`, optional, \
    default: `NoneType`
        Relationship with ``calculated_values``.
    central_contacts : `clinical_trials.orm.CentralContact`, optional, \
    default: `NoneType`
        Relationship with ``central_contacts``.
    conditions : `clinical_trials.orm.Condition`, optional, default: \
    `NoneType`
        Relationship with ``conditions``.
    countries : `clinical_trials.orm.Country`, optional, default: `NoneType`
        Relationship with ``countries``.
    design_group_interventions : \
    `clinical_trials.orm.DesignGroupIntervention`, optional, default: \
    `NoneType`
        Relationship with ``design_group_interventions``.
    design_groups : `clinical_trials.orm.DesignGroup`, optional, default: \
    `NoneType`
        Relationship with ``design_groups``.
    design_outcomes : `clinical_trials.orm.DesignOutcome`, optional, default: \
    `NoneType`
        Relationship with ``design_outcomes``.
    designs : `clinical_trials.orm.Design`, optional, default: `NoneType`
        Relationship with ``designs``.
    detailed_descriptions : `clinical_trials.orm.DetailedDescription`, \
    optional, default: `NoneType`
        Relationship with ``detailed_descriptions``.
    documents : `clinical_trials.orm.Document`, optional, default: `NoneType`
        Relationship with ``documents``.
    drop_withdrawals : `clinical_trials.orm.DropWithdrawal`, optional, \
    default: `NoneType`
        Relationship with ``drop_withdrawals``.
    eligibilities : `clinical_trials.orm.Eligibility`, optional, default: \
    `NoneType`
        Relationship with ``eligibilities``.
    facilities : `clinical_trials.orm.Facility`, optional, default: \
    `NoneType`
        Relationship with ``facilities``.
    facility_contacts : `clinical_trials.orm.FacilityContact`, optional, \
    default: `NoneType`
        Relationship with ``facility_contacts``.
    facility_investigators : `clinical_trials.orm.FacilityInvestigator`, \
    optional, default: `NoneType`
        Relationship with ``facility_investigators``.
    id_information : `clinical_trials.orm.IdInformation`, optional, default: \
    `NoneType`
        Relationship with ``id_information``.
    intervention_other_names : `clinical_trials.orm.InterventionOtherName`, \
    optional, default: `NoneType`
        Relationship with ``intervention_other_names``.
    interventions : `clinical_trials.orm.Intervention`, optional, default: \
    `NoneType`
        Relationship with ``interventions``.
    ipd_information_types : `clinical_trials.orm.IpdInformationType`, \
    optional, default: `NoneType`
        Relationship with ``ipd_information_types``.
    keywords : `clinical_trials.orm.Keyword`, optional, default: `NoneType`
        Relationship with ``keywords``.
    links : `clinical_trials.orm.Link`, optional, default: `NoneType`
        Relationship with ``links``.
    milestones : `clinical_trials.orm.Milestone`, optional, default: \
    `NoneType`
        Relationship with ``milestones``.
    outcome_analyses : `clinical_trials.orm.OutcomeAnalysis`, optional, \
    default: `NoneType`
        Relationship with ``outcome_analyses``.
    outcome_analysis_groups : `clinical_trials.orm.OutcomeAnalysisGroup`, \
    optional, default: `NoneType`
        Relationship with ``outcome_analysis_groups``.
    outcome_counts : `clinical_trials.orm.OutcomeCount`, optional, default: \
    `NoneType`
        Relationship with ``outcome_counts``.
    outcome_measurements : `clinical_trials.orm.OutcomeMeasurement`, \
    optional, default: `NoneType`
        Relationship with ``outcome_measurements``.
    outcomes : `clinical_trials.orm.Outcome`, optional, default: `NoneType`
        Relationship with ``outcomes``.
    overall_officials : `clinical_trials.orm.OverallOfficial`, optional, \
    default: `NoneType`
        Relationship with ``overall_officials``.
    participant_flows : `clinical_trials.orm.ParticipantFlow`, optional, \
    default: `NoneType`
        Relationship with ``participant_flows``.
    pending_results : `clinical_trials.orm.PendingResult`, optional, default: \
    `NoneType`
        Relationship with ``pending_results``.
    provided_documents : `clinical_trials.orm.ProvidedDocument`, optional, \
    default: `NoneType`
        Relationship with ``provided_documents``.
    reported_events : `clinical_trials.orm.ReportedEvent`, optional, default: \
    `NoneType`
        Relationship with ``reported_events``.
    reported_event_totals : `clinical_trials.orm.ReportedEventTotal`, \
    optional, default: `NoneType`
        Relationship with ``reported_event_totals``.
    responsible_parties : `clinical_trials.orm.ResponsibleParty`, optional, \
    default: `NoneType`
        Relationship with ``responsible_parties``.
    result_agreements : `clinical_trials.orm.ResultAgreement`, optional, \
    default: `NoneType`
        Relationship with ``result_agreements``.
    result_contacts : `clinical_trials.orm.ResultContact`, optional, default: \
    `NoneType`
        Relationship with ``result_contacts``.
    result_groups : `clinical_trials.orm.ResultGroup`, optional, default: \
    `NoneType`
        Relationship with ``result_groups``.
    retractions : `clinical_trials.orm.Retraction`, optional, default: \
    `NoneType`
        Relationship with ``retractions``.
    search_results : `clinical_trials.orm.SearchResult`, optional, default: \
    `NoneType`
        Relationship with ``search_results``.
    sponsors : `clinical_trials.orm.Sponsor`, optional, default: `NoneType`
        Relationship with ``sponsors``.
    study_references : `clinical_trials.orm.StudyReference`, optional, \
    default: `NoneType`
        Relationship with ``study_references``.

    Notes
    -----
    Basic info about study, including study title, date study registered with
    ClinicalTrials.gov, date results first posted to ClinicalTrials.gov, dates
    for study start and completion, phase of study, enrollment status, planned
    or actual enrollment, number of study arms/groups, etc. This is part of the
    ``ctgov`` schema. This is part of the Protocol & Results domain. This maps
    to `one` row in the ``studies`` table.
    """
    __tablename__ = "studies"

    nct_id = Column(
        String(20), Sequence("nct_id_seq"), index=False, unique=False,
        nullable=False, primary_key=True,
        doc="NCT ID is a unique identification code given to each clinical "
        "study registered on ClinicalTrials.gov. The format is the letters "
        "'NCT' followed by an 8-digit number (for example, NCT00000419)."
    )
    nlm_download_date_description = Column(
        String(255), index=False, unique=False, nullable=True,
        doc="Date NLM made study available via their API."
    )
    study_first_submitted_date = Column(
        Date, index=False, unique=False, nullable=False,
        doc="The _submitted suffix indicates the date that the responsible "
        "party pressed the release button in the ClinicalTrials.gov data "
        "submission system, thereby submitting the data for review prior to "
        "posting."
    )
    results_first_submitted_date = Column(
        Date, index=False, unique=False, nullable=True,
        doc="This value is only available for studies that have results. It "
        "is the date when the results were first received. The _submitted "
        "suffix indicates the date that the responsible party pressed the "
        "release button in the ClinicalTrials.gov data submission system, "
        "thereby submitting the data for review prior to posting."
    )
    disposition_first_submitted_date = Column(
        Date, index=False, unique=False, nullable=True,
        doc="Date when a sponsor indicates that they will be delaying posting"
        " of results (and are certifying initial approval or new use), or when"
        " they apply for an extension of the deadline. The _submitted suffix "
        "indicates the date that the responsible party pressed the release "
        "button in the ClinicalTrials.gov data submission system, thereby "
        "submitting the data for review prior to posting."
    )
    last_update_submitted_date = Column(
        Date, index=False, unique=False, nullable=False,
        doc="The _submitted suffix indicates the date that the responsible "
        "party pressed the release button in the ClinicalTrials.gov data "
        "submission system, thereby submitting the data for review prior to "
        "posting."
    )
    study_first_submitted_qc_date = Column(
        Date, index=False, unique=False, nullable=False,
        doc="The _submitted_qc suffix indicates the submission date of the "
        "version of the record that met quality control criteria. It is common"
        " for there to be revisions between the first submission and the "
        "submission that meets qc criteria. Note this is not the date that the"
        " qc review was completed."
    )
    study_first_posted_date = Column(
        Date, index=False, unique=False, nullable=False,
        doc="The _posted suffix indicates the date that a submission was made"
        " public on ClinicalTrials.gov. Typically, posting occurs the business"
        " day after qc review was successfully completed. We began collecting "
        "posted dates in early 2017. Dates from before then are estmates. All "
        "posted dates include a type attribute of Actual or Estimate."
    )
    study_first_posted_date_type = Column(
        String(20), index=False, unique=False, nullable=False
    )
    results_first_submitted_qc_date = Column(
        Date, index=False, unique=False, nullable=True,
        doc="The _submitted_qc suffix indicates the submission date of the "
        "version of the record that met quality control criteria. It is common"
        " for there to be revisions between the first submission and the "
        "submission that meets qc criteria. Note this is not the date that the"
        " qc review was completed."
    )
    results_first_posted_date = Column(
        Date, index=False, unique=False, nullable=True,
        doc="The _posted suffix indicates the date that a submission was made"
        " public on ClinicalTrials.gov. Typically, posting occurs the business"
        " day after qc review was successfully completed. We began collecting "
        "posted dates in early 2017. Dates from before then are estmates. All "
        "posted dates include a type attribute of Actual or Estimate."
    )
    results_first_posted_date_type = Column(
        String(20), index=False, unique=False, nullable=True
    )
    disposition_first_submitted_qc_date = Column(
        Date, index=False, unique=False, nullable=True,
        doc="The _submitted_qc suffix indicates the submission date of the "
        "version of the record that met quality control criteria. It is common"
        " for there to be revisions between the first submission and the "
        "submission that meets qc criteria. Note this is not the date that the"
        " qc review was completed."
    )
    disposition_first_posted_date = Column(
        Date, index=False, unique=False, nullable=True,
        doc="The _posted suffix indicates the date that a submission was made"
        " public on ClinicalTrials.gov. Typically, posting occurs the business"
        " day after qc review was successfully completed. We began collecting "
        "posted dates in early 2017. Dates from before then are estmates. All "
        "posted dates include a type attribute of Actual or Estimate."
    )
    disposition_first_posted_date_type = Column(
        String(20), index=False, unique=False, nullable=True
    )
    last_update_submitted_qc_date = Column(
        Date, index=False, unique=False, nullable=False,
        doc="The _submitted_qc suffix indicates the submission date of the "
        "version of the record that met quality control criteria. It is common"
        " for there to be revisions between the first submission and the "
        "submission that meets qc criteria. Note this is not the date that the"
        " qc review was completed."
    )
    last_update_posted_date = Column(
        Date, index=False, unique=False, nullable=False,
        doc="From a utility standpoint, the new <last_update_posted> date is "
        "likely the date that most people want to search. We previously did "
        "not have this date available and the old <lastchanged_date> was the "
        "closest alternative, though problematic because it never meant what "
        "was implied by the name. The _posted suffix indicates the date that a"
        " submission was made public on ClinicalTrials.gov. Typically, posting"
        " occurs the business day after qc review was successfully completed. "
        "We began collecting posted dates in early 2017. Dates from before "
        "then are estmates. All posted dates include a type attribute of "
        "Actual or Estimate."
    )
    last_update_posted_date_type = Column(
        String(20), index=False, unique=False, nullable=False
    )
    start_month_year = Column(
        String(20), index=False, unique=False, nullable=True,
        doc="The value of the start date exactly as it is provided by "
        "ClinicalTrials.gov. Some studies have only month/year defined for the"
        " start date (ie. October, 2014). That value is stored in this column "
        "and the start_date column converts this value to a date by assigning "
        "it the first day of the month."
    )
    start_date_type = Column(
        String(20), index=False, unique=False, nullable=True
    )
    start_date = Column(
        Date, index=False, unique=False, nullable=True,
        doc="The date the study started in a date-type format so that it can "
        "be used to find studies before/after/inclusive of a date or dates. "
        "For studies that only provide month/year, the last day of the month "
        "is used."
    )
    verification_month_year = Column(
        String(20), index=False, unique=False, nullable=True,
        doc="The value of the verification date exactly as it is provided by "
        "ClinicalTrials.gov. Some studies have only month/year defined for the"
        " verification date (ie. October, 2014). That value is stored in this "
        "column and the verification_date column converts this value to a date"
        " by assigning it the first day of the month."
    )
    verification_date = Column(
        Date, index=False, unique=False, nullable=True,
        doc="The date the study was last verified in a date-type format so "
        "that it can be used to find studies before/after/inclusive of a date "
        "or dates. For studies that only provide month/year, the last day of "
        "the month is used."
    )
    completion_month_year = Column(
        String(20), index=False, unique=False, nullable=True,
        doc="The value of the completion date exactly as it is provided by "
        "ClinicalTrials.gov. Some studies have only month/year defined for the"
        " completion date (ie. October, 2014). That value is stored in this "
        "column and the completion_date column converts this value to a date "
        "by assigning it the first day of the month."
    )
    completion_date_type = Column(
        String(20), index=False, unique=False, nullable=True
    )
    completion_date = Column(
        Date, index=False, unique=False, nullable=True,
        doc="The date the study was completed (as defined by NLM) in a date-"
        "type format so that it can be used to find studies "
        "before/after/inclusive of a date or dates. For studies that only "
        "provide month/year, the last day of the month is used."
    )
    primary_completion_month_year = Column(
        String(20), index=False, unique=False, nullable=True,
        doc="The value of the primary completion date exactly as it is "
        "provided by ClinicalTrials.gov. Some studies have only month/year "
        "defined for the primary completion date (ie. October, 2014). That "
        "value is stored in this column and the primary_completion_date column"
        " converts this value to a date by assigning it the first day of the "
        "month."
    )
    primary_completion_date_type = Column(
        String(20), index=False, unique=False, nullable=True
    )
    primary_completion_date = Column(
        Date, index=False, unique=False, nullable=True,
        doc="The primary completion date for the study (as defined by NLM) in"
        " a date-type format so that it can be used to find studies "
        "before/after/inclusive of a date or dates. For studies that only "
        "provide month/year, the last day of the month is used."
    )
    target_duration = Column(
        String(20), index=False, unique=False, nullable=True
    )
    study_type = Column(
        String(40), index=False, unique=False, nullable=True
    )
    acronym = Column(
        String(20), index=False, unique=False, nullable=True
    )
    baseline_population = Column(
        String(560), index=False, unique=False, nullable=True
    )
    brief_title = Column(
        String(340), index=False, unique=False, nullable=False
    )
    official_title = Column(
        String(660), index=False, unique=False, nullable=True
    )
    overall_status = Column(
        String(40), index=False, unique=False, nullable=False
    )
    last_known_status = Column(
        String(40), index=False, unique=False, nullable=True
    )
    phase = Column(
        String(20), index=False, unique=False, nullable=True
    )
    enrollment = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="For Interventional Study Design, Enrollment is required by "
        "FDAAA. For Observational Study Design, Enrollment is required by "
        "ClinicalTrials.gov (*)."
    )
    enrollment_type = Column(
        String(20), index=False, unique=False, nullable=True
    )
    source = Column(
        String(180), index=False, unique=False, nullable=False
    )
    limitations_and_caveats = Column(
        String(255), index=False, unique=False, nullable=True
    )
    number_of_arms = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="The number of groups for interventional studies, it is NULL when"
        " the study is not an interventional study."
    )
    number_of_groups = Column(
        Integer, index=False, unique=False, nullable=True,
        doc="The number of arms for interventional studies, it is NULL when "
        "the study is an interventional study."
    )
    why_stopped = Column(
        String(280), index=False, unique=False, nullable=True
    )
    has_expanded_access = Column(
        Boolean, index=False, unique=False, nullable=True
    )
    expanded_access_type_individual = Column(
        Boolean, index=False, unique=False, nullable=True
    )
    expanded_access_type_intermediate = Column(
        Boolean, index=False, unique=False, nullable=True
    )
    expanded_access_type_treatment = Column(
        Boolean, index=False, unique=False, nullable=True
    )
    has_dmc = Column(
        Boolean, index=False, unique=False, nullable=True
    )
    is_fda_regulated_drug = Column(
        Boolean, index=False, unique=False, nullable=True
    )
    is_fda_regulated_device = Column(
        Boolean, index=False, unique=False, nullable=True
    )
    is_unapproved_device = Column(
        Boolean, index=False, unique=False, nullable=True
    )
    is_ppsd = Column(
        Boolean, index=False, unique=False, nullable=True
    )
    is_us_export = Column(
        Boolean, index=False, unique=False, nullable=True,
        doc="Product Manufactured in and Exported from the U.S. Definition: "
        "Whether any drug product (including a biological product) or device "
        "product studied in the clinical study is manufactured in the United "
        "States or one of its territories and exported for study in a clinical"
        " study in another country. Select Yes/No."
    )
    biospec_retention = Column(
        String(40), index=False, unique=False, nullable=True
    )
    biospec_description = Column(
        String(1100), index=False, unique=False, nullable=True
    )
    ipd_time_frame = Column(
        String(1100), index=False, unique=False, nullable=True
    )
    ipd_access_criteria = Column(
        String(1100), index=False, unique=False, nullable=True
    )
    ipd_url = Column(
        String(220), index=False, unique=False, nullable=True
    )
    plan_to_share_ipd = Column(
        String(20), index=False, unique=False, nullable=True
    )
    plan_to_share_ipd_description = Column(
        String(1100), index=False, unique=False, nullable=True
    )
    created_at = Column(
        String(40), index=False, unique=False, nullable=False,
        doc="(AACT assigned timestamp) Date/time the study was loaded into "
        "AACT from ClinicalTrials.gov."
    )
    updated_at = Column(
        String(40), index=False, unique=False, nullable=False,
        doc="AACT assigned timestamp) Last date/time the row was modified (At"
        " this point, this value should always be same ascreated_at."
    )
    source_class = Column(
        String(20), index=False, unique=False, nullable=True
    )
    delayed_posting = Column(
        Boolean, index=False, unique=False, nullable=True
    )
    expanded_access_nctid = Column(
        String(20), index=False, unique=False, nullable=True
    )
    expanded_access_status_for_nctid = Column(
        String(40), index=False, unique=False, nullable=True
    )
    fdaaa801_violation = Column(
        Boolean, index=False, unique=False, nullable=True
    )
    baseline_type_units_analyzed = Column(
        String(60), index=False, unique=False, nullable=True
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    baseline_counts = relationship(
        "BaselineCount", back_populates="studies",
        doc="Relationship back to ``baseline_counts``."
    )
    baseline_measurements = relationship(
        "BaselineMeasurement", back_populates="studies",
        doc="Relationship back to ``baseline_measurements``."
    )
    brief_summaries = relationship(
        "BriefSummary", back_populates="studies",
        doc="Relationship back to ``brief_summaries``."
    )
    browse_conditions = relationship(
        "BrowseCondition", back_populates="studies",
        doc="Relationship back to ``browse_conditions``."
    )
    browse_interventions = relationship(
        "BrowseIntervention", back_populates="studies",
        doc="Relationship back to ``browse_interventions``."
    )
    calculated_values = relationship(
        "CalculatedValue", back_populates="studies",
        doc="Relationship back to ``calculated_values``."
    )
    central_contacts = relationship(
        "CentralContact", back_populates="studies",
        doc="Relationship back to ``central_contacts``."
    )
    conditions = relationship(
        "Condition", back_populates="studies",
        doc="Relationship back to ``conditions``."
    )
    countries = relationship(
        "Country", back_populates="studies",
        doc="Relationship back to ``countries``."
    )
    design_group_interventions = relationship(
        "DesignGroupIntervention", back_populates="studies",
        doc="Relationship back to ``design_group_interventions``."
    )
    design_groups = relationship(
        "DesignGroup", back_populates="studies",
        doc="Relationship back to ``design_groups``."
    )
    design_outcomes = relationship(
        "DesignOutcome", back_populates="studies",
        doc="Relationship back to ``design_outcomes``."
    )
    designs = relationship(
        "Design", back_populates="studies",
        doc="Relationship back to ``designs``."
    )
    detailed_descriptions = relationship(
        "DetailedDescription", back_populates="studies",
        doc="Relationship back to ``detailed_descriptions``."
    )
    documents = relationship(
        "Document", back_populates="studies",
        doc="Relationship back to ``documents``."
    )
    drop_withdrawals = relationship(
        "DropWithdrawal", back_populates="studies",
        doc="Relationship back to ``drop_withdrawals``."
    )
    eligibilities = relationship(
        "Eligibility", back_populates="studies",
        doc="Relationship back to ``eligibilities``."
    )
    facilities = relationship(
        "Facility", back_populates="studies",
        doc="Relationship back to ``facilities``."
    )
    facility_contacts = relationship(
        "FacilityContact", back_populates="studies",
        doc="Relationship back to ``facility_contacts``."
    )
    facility_investigators = relationship(
        "FacilityInvestigator", back_populates="studies",
        doc="Relationship back to ``facility_investigators``."
    )
    id_information = relationship(
        "IdInformation", back_populates="studies",
        doc="Relationship back to ``id_information``."
    )
    intervention_other_names = relationship(
        "InterventionOtherName", back_populates="studies",
        doc="Relationship back to ``intervention_other_names``."
    )
    interventions = relationship(
        "Intervention", back_populates="studies",
        doc="Relationship back to ``interventions``."
    )
    ipd_information_types = relationship(
        "IpdInformationType", back_populates="studies",
        doc="Relationship back to ``ipd_information_types``."
    )
    keywords = relationship(
        "Keyword", back_populates="studies",
        doc="Relationship back to ``keywords``."
    )
    links = relationship(
        "Link", back_populates="studies", doc="Relationship back to ``links``."
    )
    milestones = relationship(
        "Milestone", back_populates="studies",
        doc="Relationship back to ``milestones``."
    )
    outcome_analyses = relationship(
        "OutcomeAnalysis", back_populates="studies",
        doc="Relationship back to ``outcome_analyses``."
    )
    outcome_analysis_groups = relationship(
        "OutcomeAnalysisGroup", back_populates="studies",
        doc="Relationship back to ``outcome_analysis_groups``."
    )
    outcome_counts = relationship(
        "OutcomeCount", back_populates="studies",
        doc="Relationship back to ``outcome_counts``."
    )
    outcome_measurements = relationship(
        "OutcomeMeasurement", back_populates="studies",
        doc="Relationship back to ``outcome_measurements``."
    )
    outcomes = relationship(
        "Outcome", back_populates="studies",
        doc="Relationship back to ``outcomes``."
    )
    overall_officials = relationship(
        "OverallOfficial", back_populates="studies",
        doc="Relationship back to ``overall_officials``."
    )
    participant_flows = relationship(
        "ParticipantFlow", back_populates="studies",
        doc="Relationship back to ``participant_flows``."
    )
    pending_results = relationship(
        "PendingResult", back_populates="studies",
        doc="Relationship back to ``pending_results``."
    )
    provided_documents = relationship(
        "ProvidedDocument", back_populates="studies",
        doc="Relationship back to ``provided_documents``."
    )
    reported_events = relationship(
        "ReportedEvent", back_populates="studies",
        doc="Relationship back to ``reported_events``."
    )
    reported_event_totals = relationship(
        "ReportedEventTotal", back_populates="studies",
        doc="Relationship back to ``reported_event_totals``."
    )
    responsible_parties = relationship(
        "ResponsibleParty", back_populates="studies",
        doc="Relationship back to ``responsible_parties``."
    )
    result_agreements = relationship(
        "ResultAgreement", back_populates="studies",
        doc="Relationship back to ``result_agreements``."
    )
    result_contacts = relationship(
        "ResultContact", back_populates="studies",
        doc="Relationship back to ``result_contacts``."
    )
    result_groups = relationship(
        "ResultGroup", back_populates="studies",
        doc="Relationship back to ``result_groups``."
    )
    retractions = relationship(
        "Retraction", back_populates="studies",
        doc="Relationship back to ``retractions``."
    )
    search_results = relationship(
        "SearchResult", back_populates="studies",
        doc="Relationship back to ``search_results``."
    )
    sponsors = relationship(
        "Sponsor", back_populates="studies",
        doc="Relationship back to ``sponsors``."
    )
    study_references = relationship(
        "StudyReference", back_populates="studies",
        doc="Relationship back to ``study_references``."
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class StudyReference(Base):
    """A representation of the ``study_references`` table.

    Parameters
    ----------
    id : `int`
        The ``id`` column.
    nct_id : `str`
        The ``nct_id`` column. Foreign key to ``studies.nct_id`` (length 20).
    pmid : `int`, optional, default: `NoneType`
        The ``pmid`` column.
    reference_type : `str`
        reference' if value from <results> 'results_reference' if value from
        <results_reference> (length 20).
    citation : `str`
        The ``citation`` column (length 21360).
    studies : `clinical_trials.orm.Study`, optional, default: `NoneType`
        Relationship with ``studies``.

    Notes
    -----
    Citations to publications related to the study protocol and/or results.
    Includes PubMed Unique Identifier (PMID) and/or full bibliographic
    citation. This is part of the ``ctgov`` schema. This is part of the
    Protocol domain. This maps to `many` rows in the ``studies`` table.
    """
    __tablename__ = "study_references"

    id = Column(
        Integer, Sequence("id_seq"), index=False, unique=False,
        nullable=False, primary_key=True
    )
    nct_id = Column(
        String(20), ForeignKey("studies.nct_id"), index=True, unique=False,
        nullable=False
    )
    pmid = Column(
        Integer, index=False, unique=False, nullable=True
    )
    reference_type = Column(
        String(20), index=False, unique=False, nullable=False,
        doc="reference' if value from <results> 'results_reference' if value "
        "from <results_reference>."
    )
    citation = Column(
        Text, index=False, unique=False, nullable=False
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ relationships ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    studies = relationship(
        "Study", back_populates="study_references",
        doc="Relationship back to ``studies``"
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        return print_orm_obj(self)