"""A utility for parsing the AACT schema documentation and producing a table
info and a column info files for ORM code creation. End users do not need to be
concerned with this. This was used to generate the initial ORM code for the
package.

This requires two schema documentation files. The table descriptions
`Excel file <https://aact.ctti-clinicaltrials.org/documentation/aact_tables.xlsx>`_ ,
and the column descriptions
`CSV file <https://aact.ctti-clinicaltrials.org/definitions.csv>`_ . If any of
those links do not work, please see the `schema page <https://aact.ctti-clinicaltrials.org/schema>`_.

Once downloaded, you can pass these two files to this script to produce the
info files required for the automated ORM building.
"""
# Importing the version number (in __init__.py) and the package name
from ctg import (
    __version__,
    __name__ as pkg_name
)
from ctg.aact import (
    constants as con
)
from pyaddons import (
    log,
)
from sqlalchemy_config import orm_code as oc
from openpyxl import load_workbook
import argparse
import sys
import os
import re
import csv
# import pprint as pp


_SCRIPT_NAME = "ctg-aact-parse-schema"
"""The name of the script (`str`)
"""
# Use the module docstring for argparse
_DESC = __doc__
"""The program description (`str`)
"""

_TABLE_INFO_HEADER = [
    oc.TABLE_NAME_COL, oc.TABLE_NAME_NEW_COL, oc.TABLE_CLASS_NAME_COL,
    oc.TABLE_DOC_COL
]
"""The header for the table info output file (`list` of `str`)
"""
_COLUMN_INFO_HEADER = [
    oc.TABLE_NAME_COL, oc.COLUMN_NAME_COL, oc.COLUMN_NAME_NEW_COL,
    oc.COLUMN_DTYPE, oc.COLUMN_MAX_LEN, oc.COLUMN_PRIMARY_KEY,
    oc.COLUMN_INDEX, oc.COLUMN_NULLABLE, oc.COLUMN_UNIQUE,
    oc.COLUMN_FOREIGN_KEY, oc.COLUMN_DOC
]
"""The header for the column info output file (`list` of `str`)
"""
_IN_DELIMITER = ","
"""The delimiter of the input column definition file (`str`)
"""
_OUT_DELIMITER = "\t"
"""The delimiter of the output info files (`str`)
"""

# ####### INPUT COLUMN NAMES AND HEADERS ######

# Table file
_TABLE_SCHEMA_COL = 'db schema'
"""The name of the schema column in the input table Excel file (`str`)
"""
_TABLE_NAME_COL = 'table'
"""The name of the table name column in the input table Excel file (`str`)
"""
_TABLE_DESC_COL = 'description'
"""The name of the table description column in the input table Excel file
(`str`)
"""
_TABLE_NROWS_COL = 'rows per study'
"""The name of the number of rows column in the input table Excel file
(`str`)
"""
_TABLE_DOMAIN_COL = 'domain'
"""The name of the table domain column in the input table Excel file (`str`)
"""
_TABLE_DOC_COL = 'nlm doc'
"""The name of the table documentation column in the input table Excel
file (`str`)
"""
_TABLE_FILE_HEADER = [
    _TABLE_SCHEMA_COL,
    _TABLE_NAME_COL,
    _TABLE_DESC_COL,
    _TABLE_NROWS_COL,
    _TABLE_DOMAIN_COL,
    _TABLE_DOC_COL,
]
"""The full input table file header (`list` of `str`)
"""

# Column file
_COLUMN_CTTI_DOC_COL = 'CTTI note'
"""The name of the clinical trials initiative documentation column
in the input column CSV file (`str`)
"""
_COLUMN_NAME_COL = 'column'
"""The name of the column name column in the input column CSV file (`str`)
"""
_COLUMN_DTYPE_COL = 'data type'
"""The name of the data type column in the input column CSV file (`str`)
"""
_COLUMN_SCHEMA_COL = 'db schema'
"""The name of the schema column in the input column CSV file (`str`)
"""
_COLUMN_SECTION_COL = 'db section'
"""The name of the database section column in the input column CSV
file (`str`)
"""
_COLUMN_NLM_DOC_COL = 'nlm doc'
"""The name of the NLM documentation column in the input column CSV
file (`str`)
"""
_COLUMN_NROWS_COL = 'row count'
"""The name of the number of rows column in the input column CSV file (`str`)
"""
_COLUMN_SOURCE_COL = 'source'
"""The name of the source column in the input column CSV file (`str`)
"""
_COLUMN_TABLE_COL = 'table'
"""The name of the table name column in the input column CSV file (`str`)
"""
_COLUMN_FILE_HEADER = [
    _COLUMN_CTTI_DOC_COL,
    _COLUMN_NAME_COL,
    _COLUMN_DTYPE_COL,
    _COLUMN_SCHEMA_COL,
    _COLUMN_SECTION_COL,
    _COLUMN_NLM_DOC_COL,
    _COLUMN_NROWS_COL,
    _COLUMN_SOURCE_COL,
    _COLUMN_TABLE_COL,
]
"""The full input column file header (`list` of `str`)
"""

# Errors, omissions
KNOWN_MISSING = [
    (
        'ipd_information_types',
        'IpdInformationType',
        ''
    ),
    (
        con.REPORTED_EVENTS_TOT_TABLE,
        "ReportedEventTotal",
        ''
    ),
    (
        con.RETRACTIONS_TABLE,
        "Retraction",
        ''
    )
]
"""The known tables that are missing in the AACT table Excel file, these must
be errors, each nested tuple is table name, ORM class name
(`list` of (`str`, `str`))
"""
TO_REMOVE = [
    "analyzed_studies",
    "study_searches",
    "tagged_terms",
    "cardiovascular_studies",
    "cdek_organizations",
    "cdek_synonyms",
    "mental_health_studies",
    "oncology_studies",
    "table_name",
    "tagged_terms"
]
"""A list of name names that should be removed from the output table-info and
column-info files as there are no flat files for them (`list` of `str`)
"""
TO_ADD = {
    con.REPORTED_EVENTS_TOT_TABLE: [
        {
            oc.TABLE_NAME_COL: con.REPORTED_EVENTS_TOT_TABLE,
            oc.COLUMN_NAME_COL: con.ID_COL,
            oc.COLUMN_PRIMARY_KEY: True,
            oc.COLUMN_INDEX: False,
            oc.COLUMN_UNIQUE: False,
            oc.COLUMN_FOREIGN_KEY: None,
            oc.COLUMN_DOC: "The AACT primary key column."
        },
        {
            oc.TABLE_NAME_COL: con.REPORTED_EVENTS_TOT_TABLE,
            oc.COLUMN_NAME_COL: con.NCT_ID_COL,
            oc.COLUMN_PRIMARY_KEY: False,
            oc.COLUMN_INDEX: False,
            oc.COLUMN_UNIQUE: False,
            oc.COLUMN_FOREIGN_KEY: [
                (con.REPORTED_EVENTS_TOT_TABLE, con.NCT_ID_COL),
                (con.STUDIES_TABLE, con.NCT_ID_COL)
            ],
            oc.COLUMN_DOC: "The clinicaltrials.gov study identifier."
        },
        {
            oc.TABLE_NAME_COL: con.REPORTED_EVENTS_TOT_TABLE,
            oc.COLUMN_NAME_COL: con.GROUP_CODE_COL,
            oc.COLUMN_PRIMARY_KEY: False,
            oc.COLUMN_INDEX: False,
            oc.COLUMN_UNIQUE: False,
            oc.COLUMN_FOREIGN_KEY: None,
            # oc.COLUMN_FOREIGN_KEY: [
            #     (con.REPORTED_EVENTS_TOT_TABLE, con.GROUP_CODE_COL),
            #     (con.RESULT_GROUP_TABLE, con.GROUP_CODE_COL)
            # ],
            oc.COLUMN_DOC: "The clinicaltrials.gov result group code."
        },
        {
            oc.TABLE_NAME_COL: con.REPORTED_EVENTS_TOT_TABLE,
            oc.COLUMN_NAME_COL: "event_type",
            oc.COLUMN_PRIMARY_KEY: False,
            oc.COLUMN_INDEX: False,
            oc.COLUMN_UNIQUE: False,
            oc.COLUMN_FOREIGN_KEY: None,
            oc.COLUMN_DOC: "The event type, i.e. serious or death."
        },
        {
            oc.TABLE_NAME_COL: con.REPORTED_EVENTS_TOT_TABLE,
            oc.COLUMN_NAME_COL: "classification",
            oc.COLUMN_PRIMARY_KEY: False,
            oc.COLUMN_INDEX: False,
            oc.COLUMN_UNIQUE: False,
            oc.COLUMN_FOREIGN_KEY: None,
            oc.COLUMN_DOC: "The event type classification."
        },
        {
            oc.TABLE_NAME_COL: con.REPORTED_EVENTS_TOT_TABLE,
            oc.COLUMN_NAME_COL: "subjects_affected",
            oc.COLUMN_PRIMARY_KEY: False,
            oc.COLUMN_INDEX: False,
            oc.COLUMN_UNIQUE: False,
            oc.COLUMN_FOREIGN_KEY: None,
            oc.COLUMN_DOC: (
                "The number of subjects in the study that had the event."
            )
        },
        {
            oc.TABLE_NAME_COL: con.REPORTED_EVENTS_TOT_TABLE,
            oc.COLUMN_NAME_COL: "subjects_at_risk",
            oc.COLUMN_PRIMARY_KEY: False,
            oc.COLUMN_INDEX: False,
            oc.COLUMN_UNIQUE: False,
            oc.COLUMN_FOREIGN_KEY: None,
            oc.COLUMN_DOC: (
                "The number of subjects in the study at risk of the event."
            )
        },
        {
            oc.TABLE_NAME_COL: con.REPORTED_EVENTS_TOT_TABLE,
            oc.COLUMN_NAME_COL: "created_at",
            oc.COLUMN_PRIMARY_KEY: False,
            oc.COLUMN_INDEX: False,
            oc.COLUMN_UNIQUE: False,
            oc.COLUMN_FOREIGN_KEY: None,
            oc.COLUMN_DOC: "The datetime the row was created."
        },
        {
            oc.TABLE_NAME_COL: con.REPORTED_EVENTS_TOT_TABLE,
            oc.COLUMN_NAME_COL: "updated_at",
            oc.COLUMN_PRIMARY_KEY: False,
            oc.COLUMN_INDEX: False,
            oc.COLUMN_UNIQUE: False,
            oc.COLUMN_FOREIGN_KEY: None,
            oc.COLUMN_DOC: "The datetime the row was updated."
        }
    ],
    con.RETRACTIONS_TABLE: [
        {
            oc.TABLE_NAME_COL: con.RETRACTIONS_TABLE,
            oc.COLUMN_NAME_COL: con.ID_COL,
            oc.COLUMN_PRIMARY_KEY: True,
            oc.COLUMN_INDEX: False,
            oc.COLUMN_UNIQUE: False,
            oc.COLUMN_FOREIGN_KEY: None,
            oc.COLUMN_DOC: "The AACT primary key column."
        },
        {
            oc.TABLE_NAME_COL: con.RETRACTIONS_TABLE,
            oc.COLUMN_NAME_COL: "reference_id",
            oc.COLUMN_PRIMARY_KEY: False,
            oc.COLUMN_INDEX: False,
            oc.COLUMN_UNIQUE: False,
            oc.COLUMN_FOREIGN_KEY: None,
            oc.COLUMN_DOC: None
        },
        {
            oc.TABLE_NAME_COL: con.RETRACTIONS_TABLE,
            oc.COLUMN_NAME_COL: "pmid",
            oc.COLUMN_PRIMARY_KEY: False,
            oc.COLUMN_INDEX: False,
            oc.COLUMN_UNIQUE: False,
            oc.COLUMN_FOREIGN_KEY: None,
            oc.COLUMN_DOC: "The pubmed identifier of the retraction."
        },
        {
            oc.TABLE_NAME_COL: con.RETRACTIONS_TABLE,
            oc.COLUMN_NAME_COL: "source",
            oc.COLUMN_PRIMARY_KEY: False,
            oc.COLUMN_INDEX: False,
            oc.COLUMN_UNIQUE: False,
            oc.COLUMN_FOREIGN_KEY: None,
            oc.COLUMN_DOC: None
        },
        {
            oc.TABLE_NAME_COL: con.RETRACTIONS_TABLE,
            oc.COLUMN_NAME_COL: con.NCT_ID_COL,
            oc.COLUMN_PRIMARY_KEY: False,
            oc.COLUMN_INDEX: False,
            oc.COLUMN_UNIQUE: False,
            oc.COLUMN_FOREIGN_KEY: [
                (con.RETRACTIONS_TABLE, con.NCT_ID_COL),
                (con.STUDIES_TABLE, con.NCT_ID_COL)
            ],
            oc.COLUMN_DOC: "The clinicaltrials.gov study identifier."
        }
    ]

}
"""Additional column definitions that need adding as they are missing from the
documentation, note that these are only added if missing so will but be
duplicated if the docs are fixed, the keys are table names and the values are
lists of columns dicts with keys being the column-info column names (`dict`)
"""

# Set csv to max width
csv.field_size_limit(sys.maxsize)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(
        _SCRIPT_NAME, verbose=args.verbose
    )
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    try:
        table_info_path, column_info_path = parse_ctg_aact_schema(
            args.table_file, args.column_file,
            table_info_path=args.out_table_info,
            column_info_path=args.out_column_info
        )
        logger.info(f"table info written to: {table_info_path}")
        logger.info(f"column info written to: {column_info_path}")
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        log.log_interrupt(logger)
    finally:
        log.log_end(logger)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )

    parser.add_argument(
        'table_file', type=str,
        help="The table schema documentation Excel file."
    )
    parser.add_argument(
        'column_file', type=str,
        help="The column schema documentation CSV file."
    )
    parser.add_argument(
        '-t', '--out-table-info', type=str,
        help="The path to the table info output file. If not supplied then a "
        "default file name of ``<ChEMBL_version>_table_info.txt`` will be used"
        " and written to the current directory."
    )
    parser.add_argument(
        '-c', '--out-column-info', type=str,
        help="The path to the column info output file. If not supplied then a "
        "default file name of ``<ChEMBL_version>_column_info.txt`` will be"
        " used and written to the current directory."
    )
    parser.add_argument(
        '-v', '--verbose',  action="count", default=0,
        help="give more output, -vv will turn on progress monitoring"
    )

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_ctg_aact_schema(schema_table_file, schema_definitions_file,
                          table_info_path=None, column_info_path=None):
    """Parse the AACT clinicaltrials.gov schema files to produce ORM info files
    for automated ORM code production.

    Parameters
    ----------
    schema_table_file : `str`
        The path to the schema file that describes the table names and
        descriptions. This is an Excel spreadsheet.
    schema_definitions_file : `str`
        The path to the schema file that describes the column information/
        documentation. This is a CSV file.
    table_info_path : `str`, optional, default: `NoneType`
        The path to the table info output file. If not supplied then
        ``<ChEMBL_version>_table_info.txt`` will be used and written to the
        current directory.
    column_info_path : `str`, optional, default: `NoneType`
        The path to the column info output file. If not supplied then
        ``<ChEMBL_version>_column_info.txt`` will be used and written to the
        current directory.
    """
    # Setup the output file paths if not supplied
    if table_info_path is None:
        table_info_path = os.path.join(
            os.getcwd(), "ctg_aact_table_info.txt"
        )
    if column_info_path is None:
        column_info_path = os.path.join(
            os.getcwd(), "ctg_aact_column_info.txt"
        )

    # Now process the table definition files
    # This will hold all the table names encountered
    tables = set()
    with open(table_info_path, 'wt') as outtable:
        writer = csv.writer(
            outtable, delimiter=_OUT_DELIMITER,
            lineterminator=os.linesep
        )
        writer.writerow(_TABLE_INFO_HEADER)

        # tn = table name, cn = class name, td = table description
        for tn, cn, td in parse_table_file(schema_table_file):
            # Register the table and output
            tables.add(tn)
            writer.writerow([tn, None, cn, td])

    # Process the column definitions into the column-info file
    with open(column_info_path, 'wt') as outcols:
        writer = csv.DictWriter(
            outcols, _COLUMN_INFO_HEADER, delimiter=_OUT_DELIMITER,
            lineterminator=os.linesep
        )
        writer.writeheader()
        for row in parse_column_file(schema_definitions_file):
            # Make sure the table of the column definitions is present in the
            # table file
            if row[oc.TABLE_NAME_COL] not in tables:
                print("known tables: ", file=sys.stderr)
                for i in sorted(tables):
                    print(i, file=sys.stderr)
                raise KeyError(
                    f"unknown column->table: {row[oc.TABLE_NAME_COL]}"
                )

            # Make sure that the nct_id/id columns are correctly assigned
            # PK/FK
            _test_keys(row)

            # Turn any Boolean values into integer Boolean values
            for k, v in row.items():
                if v in [True, False]:
                    row[k] = int(v)

            # Any foreign key definitions for output
            if row[oc.COLUMN_FOREIGN_KEY] is not None:
                # We only output the target table
                row[oc.COLUMN_FOREIGN_KEY] = \
                    '.'.join(row[oc.COLUMN_FOREIGN_KEY][1])
            writer.writerow(row)

    return table_info_path, column_info_path


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_table_file(table_file):
    """Parse the AACT clinicaltrials.gov schema table file.

    Parameters
    ----------
    table_file : `str`
        The path to the schema file that describes the table names and
        descriptions. This is an Excel spreadsheet.

    Yields
    ------
    table_name : `str`
        The name of the table.
    class_name : `str`
        The table class name, plural table names are made singular (in a very
        rough and ready kind of way).
    table_desc : `str`
        The table description if available.
    """
    # This will keep track of all the tables we have come across, so we can
    # see if we need to output any of the manual fixes
    all_tables = set()

    # Load the spreadsheet
    workbook = load_workbook(filename=table_file)
    sheet = workbook.worksheets[0]

    # Grab the header, there are trailing cells that need to be removed
    # but we check the header is valid
    row_gen = sheet.rows
    header = [i.value for i in next(row_gen)][:len(_TABLE_FILE_HEADER)]
    if header != _TABLE_FILE_HEADER:
        raise ValueError("unexpected header: {0}".format(",".join(header)))

    # The column number of the table description column
    doc_col = header.index(_TABLE_DOC_COL)

    # This is a counter for consecutive all None rows as there are trailing
    # cells after the last row, if we encounter 10 all None rows we exit as we
    # should be at the end
    all_none = 0
    for row in row_gen:
        # Are all the cells None
        if all([i.value is None for i in row]):
            all_none += 1

            # Are we within the limit?
            if all_none < 10:
                continue
            break
        else:
            # Reset as not consecutive
            all_none = 0

        # The rows are Excel cell objects, we extract all the values from them
        row_values = dict(
            [(c, v.value) for c, v in zip(_TABLE_FILE_HEADER, row)]
        )

        # The table description
        row_values[_TABLE_DESC_COL] = _parse_desc(row_values[_TABLE_DESC_COL])

        # Add the schema documentation
        row_values[_TABLE_DESC_COL] += (
            f" This is part of the ``{row_values[_TABLE_SCHEMA_COL]}`` schema."
        )

        # Add the table domain documentation (whatever that is!)
        if row_values[_TABLE_DOMAIN_COL] not in con.MISSING_DATA:
            row_values[_TABLE_DESC_COL] += (
                f" This is part of the {row_values[_TABLE_DOMAIN_COL]}"
                " domain."
            )

        # Add the nature of the relationship to the study table to the
        # documentation
        if row_values[_TABLE_NROWS_COL] == 'many':
            row_values[_TABLE_DESC_COL] += (
                " This maps to `many` rows in the ``studies`` table."
            )
        elif row_values[_TABLE_NROWS_COL] == 'none':
            row_values[_TABLE_DESC_COL] += (
                " This maps to `no` rows in the ``studies`` table."
            )
        elif row_values[_TABLE_NROWS_COL] == 'one':
            row_values[_TABLE_DESC_COL] += (
                " This maps to `one` row in the ``studies`` table."
            )
        elif row_values[_TABLE_NROWS_COL] not in con.MISSING_DATA:
            raise ValueError(
                f"unknown '{_TABLE_NROWS_COL}' value: "
                f"{row_values[_TABLE_NROWS_COL]}"
            )

        try:
            # Add any hyperlinks back to the NLM documentation
            hl = row[doc_col].hyperlink.target
            row_values[_TABLE_DOC_COL] = re.sub(
                r'{0}(\s+-\s*)?\s*'.format(re.escape(hl)),
                '', row_values[_TABLE_DOC_COL]
            )
            row_values[_TABLE_DESC_COL] += (
                f" See `{row_values[_TABLE_DOC_COL]} <{hl}>`_ for "
                "documentation."
            )
        except AttributeError:
            pass

        # Extract the table name
        tn = row_values[_TABLE_NAME_COL].lower().strip()
        all_tables.add(tn)

        # Do we want to remove the table, some tables are present in the
        # schema but do not have flat files.
        if tn in TO_REMOVE:
            continue

        yield tn, _class_name(tn), row_values[_TABLE_DESC_COL]

    # Now for tables which are present in the download but missing in the docs
    # (presumably an oversight), output them if they have not been processed
    # already (i.e. docs might have been fixed in future)
    for tn, cn, d in KNOWN_MISSING:
        if tn not in all_tables:
            yield tn, cn, d


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_column_file(column_file):
    """Parse the AACT clinicaltrials.gov schema column file.

    Parameters
    ----------
    column_file : `str`
        The path to the schema file that describes the column names and
        descriptions. This is a CSV file.

    Yields
    ------
    row : `dict`
        The keys are column names for a valid column-info file.

    Notes
    -----
    This will attempt to infer foreign key relationships based on table/column
    naming conventions.
    """
    col_data = dict()
    with open(column_file, 'rt') as infile:
        reader = csv.DictReader(infile, delimiter=_IN_DELIMITER)
        for row in reader:
            # Initialise a blank output row
            col_row = dict([(i, None) for i in _COLUMN_INFO_HEADER])
            col_row[oc.COLUMN_PRIMARY_KEY] = False
            col_row[oc.COLUMN_INDEX] = False
            col_row[oc.COLUMN_UNIQUE] = False

            # Set the table/column names
            col_row[oc.TABLE_NAME_COL] = row[_COLUMN_TABLE_COL].strip()
            col_row[oc.COLUMN_NAME_COL] = row[_COLUMN_NAME_COL].strip()

            # Are we ignoring the table, if so we do not store
            if col_row[oc.TABLE_NAME_COL] in TO_REMOVE:
                continue

            # ID any primary key definitions, the indicator strings will be
            # removed from the documentation column
            row[_COLUMN_CTTI_DOC_COL], col_row[oc.COLUMN_PRIMARY_KEY] = \
                _is_primary_key(
                    row[_COLUMN_CTTI_DOC_COL]
                )

            # ID any foreign key definitions, the indicator strings will be
            # removed from the documentation column
            row[_COLUMN_CTTI_DOC_COL], is_foreign, source_table, \
                target_table = _is_foreign_key(row)

            # If we have a foreign key then set the source and target tables
            if is_foreign is True:
                col_row[oc.COLUMN_FOREIGN_KEY] = [source_table, target_table]

            # If the documentation is defined, then parse it
            if row[_COLUMN_CTTI_DOC_COL] not in con.MISSING_DATA:
                col_row[oc.COLUMN_DOC] = _parse_desc(row[_COLUMN_CTTI_DOC_COL])

            try:
                # Here we build the table column structure
                col_data[col_row[oc.TABLE_NAME_COL]][
                    col_row[oc.COLUMN_NAME_COL]
                ] = col_row
            except KeyError:
                # Table not present yet, so add it
                col_data.setdefault(col_row[oc.TABLE_NAME_COL], dict())
                col_data[col_row[oc.TABLE_NAME_COL]][
                    col_row[oc.COLUMN_NAME_COL]
                ] = col_row

    # Now we have parsed all the columns we make sure the foreign key
    # definitions are correct, i.e. the tables/columns can be found.
    for tn, columns in col_data.items():
        for cn, c in columns.items():
            if c[oc.COLUMN_FOREIGN_KEY] is not None:
                # tt = target table, tc = target column
                tt, tc = c[oc.COLUMN_FOREIGN_KEY][1]
                try:
                    # Can we find the table/column
                    _test_fk(tt, tc, c, col_data)
                    yield c
                    continue
                except KeyError:
                    # Can't find it, this could be because the fk column def is
                    # singular (used to estimate the fk table) and the table is
                    # plural
                    pass
                # so we pluralise and test again, if we can't find this time a
                # KeyError will be raised
                tt = _pluralise(tt)
                _test_fk(tt, tc, c, col_data)

                # Now update the foreign key defs
                c[oc.COLUMN_FOREIGN_KEY] = [
                    c[oc.COLUMN_FOREIGN_KEY][0], (tt, tc)
                ]
            yield c

    # Now add any manual columns that were missing from the docs (assuming
    # they have not been fixed)
    for tn, columns in TO_ADD.items():
        for c in columns:
            try:
                col_data[tn][c[oc.COLUMN_NAME_COL]]
            except KeyError:
                yield c


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _class_name(tn):
    """Get a class name for a table name. The class name has all underscores
    removed and is camel case. In addition classes are singular not plural.

    Parameters
    ----------
    tn : `str`
        The table name.

    Returns
    -------
    class_name : `str`
        The class name for the table name.
    """
    # Make sure the table name is expressed in singular
    tn = _unpluralise(tn.lower().strip())

    # Assemble the class name
    c = ""
    for i in re.sub(r'_+', '_', tn).split('_'):
        c += i.title()
    return c


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _pluralise(tn):
    """Make sure a table name is expressed in plural.

    Parameters
    ----------
    tn : `str`
        The table name.

    Returns
    -------
    plural_tn : `str`
        The pluralised table name.

    Notes
    -----
    This is not optimal, only handles y->ies, sis->ses and adding an s on if
    not present. It is not smart what so ever!
    """
    if tn.endswith('y'):
        return tn[:-1] + 'ies'
    elif tn.endswith('sis'):
        return re.sub(r'sis$', 'ses', tn)
    return tn + 's'


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _unpluralise(tn):
    """Make sure a table name is expressed in signular.

    Parameters
    ----------
    tn : `str`
        The table name.

    Returns
    -------
    singular_tn : `str`
        The un-pluralised table name.

    Notes
    -----
    This is not optimal, only handles ies->y, ses->sis and removing an s on if
    present. It is not smart what so ever!
    """
    if tn.endswith('ies'):
        return re.sub(r'ies$', 'y', tn)
    elif tn.endswith('ses'):
        return re.sub(r'ses$', 'sis', tn)
    elif tn.endswith('s'):
        return re.sub(r's$', '', tn)
    return tn


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _test_fk(tn, cn, col, tables):
    """Test that a foreign key target table/column exists.

    Parameters
    ----------
    tn : `str`
        The target table name.
    cn : `str`
        The target column name.
    col : `dict`
        The column from with the target table/column is derived. The dictionary
        should have the structure of a column-info file.
    tables : `dict of `dict`
        The full complement of tables that have been parsed, these are used for
        lookups of the target table/target column.

    Raises
    ------
    KeyError
        If the target table or column can't be cound in the full complement of
        tables and columns.
    """
    try:
        t = tables[tn]
    except KeyError:
        raise KeyError(
            f"target table does not exists for FK: {tn}.{cn} in "
            f"{col[oc.TABLE_NAME_COL]}.{col[oc.COLUMN_NAME_COL]}"
        )
    try:
        t[cn]
    except KeyError:
        raise KeyError(
            f"target column does not exists for FK: {tn}.{cn} in "
            f"{col[oc.TABLE_NAME_COL]}.{col[oc.COLUMN_NAME_COL]}"
        )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _test_keys(row):
    """Make sure the standard keys are set, I have noticed some omissions in
    the AACT docs.

    Parameters
    ----------
    row : `dict`
        An output column-info row. The keys are the column-info keys and the
        values are the column-info values.
    """
    # If the column is the nct_id but not the actual studies table, then we
    # make the studies table a foreign key
    if row[oc.COLUMN_NAME_COL] == con.NCT_ID_COL and \
       row[oc.TABLE_NAME_COL] != con.STUDIES_TABLE:
        row[oc.COLUMN_FOREIGN_KEY] = [
            (row[oc.TABLE_NAME_COL], row[oc.COLUMN_NAME_COL]),
            (con.STUDIES_TABLE, con.NCT_ID_COL)
        ]

    if row[oc.COLUMN_NAME_COL] == con.ID_COL:
        row[oc.COLUMN_PRIMARY_KEY] = True
        row[oc.COLUMN_DTYPE] = oc.TableColumn.Dtype.INTEGER.value


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _is_primary_key(desc):
    """Determine if a column is marked as being a primary key based on it's
    description. The primary key indicator is removed from the description.

    Parameters
    ----------
    desc : `str`
        The description to check and process.

    Returns
    -------
    desc : `str`
        The description with any primary key indicators removed.
    is_primary : `bool`
        An indicator if the description contained any primary key information.
    """
    desc = desc.strip()
    is_primary = False
    for i in [r'\(?AACT\s+assigned\s+primary\s+key[\)\.]?',
              r'\(?primary\s+key\)?\.?']:
        if re.search(i, desc, re.IGNORECASE):
            is_primary = True
            desc = re.sub(i, '', desc, flags=re.IGNORECASE)
            break
    return desc, is_primary


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _is_foreign_key(row):
    """Determine if a column is marked as being a foreign key based on it's
    description. The foreign key indicator is removed from the description.

    Parameters
    ----------
    desc : `str`
        The description to check and process.

    Returns
    -------
    desc : `str`
        The description with any primary key indicators removed.
    is_foreign : `bool`
        An indicator if the description contained any foreign key information.
    source_table : `tuple` of (`str`, `str`)
        The source table name and column for the foreign key.
    target_table : `tuple` of (`str`, `str`)
        The target table name and column for the foreign key.
    """
    desc = row[_COLUMN_CTTI_DOC_COL]
    desc = desc.strip()
    is_foreign = False
    source_table = None
    target_table = None
    for i in [r'\(?AACT foreign key to Study\)?\.?']:
        if re.search(i, desc, re.IGNORECASE):
            is_foreign = True
            desc = re.sub(i, '', desc, flags=re.IGNORECASE)
            source_table = (row[_COLUMN_TABLE_COL], row[_COLUMN_NAME_COL])
            target_table = ('studies', row[_COLUMN_NAME_COL])
            break

    for i in [r'\(?AACT assigned foreign key\)?\.?']:
        if re.search(i, desc, re.IGNORECASE):
            is_foreign = True
            desc = re.sub(i, '', desc, flags=re.IGNORECASE)
            source_table = (row[_COLUMN_TABLE_COL], row[_COLUMN_NAME_COL])

            target_table = re.sub(r'_id', '', row[_COLUMN_NAME_COL].strip())
            target_table = (target_table, 'id')
            break

    return desc, is_foreign, source_table, target_table


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_desc(desc):
    """Clean up a description string.

    Parameters
    ----------
    desc : `str`
        The description string.

    Returns
    -------
    clean_desc : `str`
        A cleaned up description string, this has internal tabs/newlines
        removed and some HTML elements that are embedded in some of the
        strings.
    """
    desc = re.sub(r'\t', ' ', desc.strip())
    desc = re.sub(r'\s+', ' ', desc)
    desc = re.sub(r'\xa0', '', desc)
    desc = re.sub(r'\n', ' ', desc)
    removed = [r'<html>', r'</html>', r'<i>', r'</i>', r'<u>', r'</u>']
    for i in removed:
        desc = re.sub(i, '', desc)

    desc = re.sub(r'Per NLM: "(.+)"', r'\1', desc)
    desc = re.sub(r'^\((.+)\)$', r'\1', desc.strip())
    desc = re.sub(r'"', "'", desc.strip())

    if not desc.endswith('.'):
        desc += '.'

    return desc


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
