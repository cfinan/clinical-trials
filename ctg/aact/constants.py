"""Some constants that may be used in multiple modules.
"""

# Some table name constants, these are only defined if I need to explicitly
# refer to a table in the code for some reason
STUDIES_TABLE = 'studies'
"""The studies table name (`str`)
"""
RESULT_GROUP_TABLE = "result_groups"
"""The result group table name (`str`)
"""
OUTCOME_TABLE = "outcomes"
"""The result group table name (`str`)
"""
OUTCOME_MEASURE_TABLE = "outcome_measurements"
"""The outcome measurements table name (`str`)
"""
REPORTED_EVENTS_TOT_TABLE = "reported_event_totals"
"""The reported_event_totals table name (`str`)
"""
RETRACTIONS_TABLE = "retractions"
"""The retractions table name (`str`)
"""
DESIGN_GROUP_TABLE = "design_groups"
"""The design groups table name (`str`)
"""
INTERVENTIONS_TABLE = "interventions"
"""The interventions table name (`str`)
"""
FACILITIES_TABLE = "facilities"
"""The facilities table name (`str`)
"""
OUTCOME_ANALYSIS_TABLE = "outcome_analyses"
"""The outcome analyses table name (`str`)
"""
TABLES = [
    STUDIES_TABLE,
    RESULT_GROUP_TABLE,
    OUTCOME_TABLE,
    OUTCOME_MEASURE_TABLE,
    DESIGN_GROUP_TABLE,
    INTERVENTIONS_TABLE,
    FACILITIES_TABLE,
    OUTCOME_ANALYSIS_TABLE,
    REPORTED_EVENTS_TOT_TABLE,
    RETRACTIONS_TABLE,
]
"""The tables in the order we want them loaded note not all tables are in
this list (`list` of `str`).
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def SORT_TABLES(x):
    """Table sorting function. Will return the integer sort position for the
    tables value.

    If the table is not represented then a tied position larger than all
    represented tables is returned.

    Parameters
    ----------
    x : `str`
        The table key value

    Returns
    -------
    int_x : `int`
        The index sort position for the table.
    """
    try:
        return TABLES.index(x)
    except ValueError:
        return len(x)


# Some column constants, these are only defined if I need to explicitly refer
# to a column in the code for some reason
NCT_ID_COL = 'nct_id'
"""The clinical trial ID column name (`str`)
"""
ID_COL = 'id'
"""The AACT ID primary key name name (`str`)
"""
GROUP_CODE_COL = 'ctgov_group_code'
"""The clinical trials.gov group code column name (`str`)
"""
MISSING_DATA = ['n/a', None, '']
"""Missing data indicators (`list` of `str` or `NoneType`)
"""
AACT_DELIMITER = '|'
"""The Delimiter of the AACT files (`str`)
"""
