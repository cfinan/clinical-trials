"""Parsers for the input tables
"""
from collections import namedtuple
from ctg.aact import type_casts as tc


Column = namedtuple("Column", ['name', 'parse', 'allow_error'])


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _BaseParser(object):
    """
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, infile):
        self._infile = infile

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        """
        """
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        """
        """
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __iter__(self):
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __next__(self):
        return {}

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self):
        """
        """
        pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self):
        """
        """
        pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class StudyParser(_BaseParser):
    """
    """
    NCT_ID = Column("ncd_id", tc.parse_ncd_id, False)
    NLM_DOWNLOAD_DATE_DESC = Column(
        "nlm_download_date_description", str, False
    )
    STUDY_FIRST_SUBMITTED_DATE = Column(
        "study_first_submitted_date", tc.parse_date, False
    )
    RESULTS_FIRST_SUBMITTED_DATE = Column(
        "results_first_submitted_date", tc.parse_date, True
    )
    DISPOSITION_FIRST_SUBMITTED_DATE = Column(
        "disposition_first_submitted_date", tc.parse_date, True
    )
    LAST_UPDATE_SUBMITTED_DATE = Column(
        "last_update_submitted_date", tc.parse_date, False
    )
    STUDY_FIRST_SUBMITTED_QC_DATE = Column(
        "study_first_submitted_qc_date", tc.parse_date, False
    )
    STUDY_FIRST_POSTED_DATE = Column(
        "study_first_posted_date", tc.parse_date, False
    )
    STUDY_FIRST_POSTED_DATE_TYPE = Column(
        "study_first_posted_date_type", str, True
    )
    RESULTS_FIRST_SUBMITTED_QC_DATE = Column(
        "results_first_submitted_qc_date", tc.parse_date, False
    )
    RESULTS_FIRST_POSTED_DATE = Column(
        "results_first_posted_date", tc.parse_date, False
    )
    RESULTS_FIRST_POSTED_DATE_TYPE = Column(
        "results_first_posted_date_type", str, False
    )
    DISPOSITION_FIRST_SUBMITTED_QC_DATE = Column(
        "disposition_first_submitted_qc_date", tc.parse_date, False
    )
    DISPOSITION_FIRST_POSTED_DATE = Column(
        "disposition_first_posted_date", tc.parse_date, False
    )
    DISPOSITION_FIRST_POSTED_DATE_TYPE = Column(
        "disposition_first_posted_date_type", str, True
    )
    # #
    LAST_UPDATE_SUBMITTED_QC_DATE = Column(
        "last_update_submitted_qc_date", tc.parse_date, False
    )
    LAST_UPDATE_POSTED_DATE = Column(
        "last_update_posted_date", tc.parse_date, False
    )
    LAST_UPDATE_POSTED_DATE_TYPE = Column(
        "last_update_posted_date_type", str, False
    )
    START_MONTH_YEAR = Column(
        "start_month_year", tc.parse_date, False
    )
    START_DATE_TYPE = Column(
        "start_date_type", str, False
    )
    START_DATE = Column(
        "start_date", tc.parse_date, False
    )
    VERIFICATION_MONTH_YEAR = Column(
        "verification_month_year", tc.parse_date, False
    )
    VERIFICATION_DATE = Column(
        "verification_date", tc.parse_date, False
    )
    COMPLETION_MONTH_YEAR = Column(
        "completion_month_year", tc.parse_date, False
    )
    COMPLETION_DATE_TYPE = Column(
        "completion_date_type", str, False
    )
    COMPLETION_DATE = Column(
        "completion_date", tc.parse_date, False
    )
    PRIMARY_COMPLETION_MONTH_YEAR = Column(
        "primary_completion_month_year", tc.parse_date, False
    )
    PRIMARY_COMPLETION_DATE_TYPE = Column(
        "primary_completion_date_type", str, False
    )
    PRIMARY_COMPLETION_DATE = Column(
        "primary_completion_date", tc.parse_date, False
    )
    TARGET_DURATION = Column(
        "target_duration", tc.parse_duration, False
    )
    STUDY_TYPE = Column(
        "study_type", str, False
    )
    ACRONYM = Column(
        "acronym", str, False
    )
    BASELINE_POPULATION = Column(
        "baseline_population", str, False
    )
    BRIEF_TITLE = Column(
        "brief_title", str, False
    )
    OFFICIAL_TITLE = Column(
        "official_title", str, False
    )
    OVERALL_STATUS = Column(
        "overall_status", str, False
    )
    LAST_KNOWN_STATUS = Column(
        "last_known_status", str, False
    )
    PHASE = Column(
        "phase", tc.parse_date, False
    )
    ENROLLMENT = Column(
        "enrollment", int, False
    )
    ENROLLMENT_TYPE = Column(
        "enrollment_type", str, False
    )
    SOURCE = Column(
        "source", str, False
    )
    LIMITATIONS_AND_CAVEATS = Column(
        "limitations_and_caveats", str, False
    )
    NUMBER_OF_ARMS = Column(
        "number_of_arms", int, False
    )
    NUMBER_OF_GROUPS = Column(
        "number_of_groups", int, False
    )
    WHY_STOPPED = Column(
        "why_stopped", tc.parse_text, False
    )
    HAS_EXPANDED_ACCESS = Column(
        "has_expanded_access", tc.parse_bool, False
    )
    EXPANDED_ACCESS_TYPE_INDIVIDUAL = Column(
        "expanded_access_type_individual", tc.parse_bool, False
    )
    EXPANDED_ACCESS_TYPE_INTERMEDIATE = Column(
        "expanded_access_type_intermediate", tc.parse_bool, False
    )
    EXPANDED_ACCESS_TYPE_TREATMENT = Column(
        "expanded_access_type_treatment", tc.parse_bool, False
    )
    HAS_DMC = Column(
        "has_dmc", tc.parse_bool, False
    )
    IS_FDA_REGULATED_DRUG = Column(
        "is_fda_regulated_drug", tc.parse_bool, False
    )
    IS_FDA_REGULATED_DEVICE = Column(
        "is_fda_regulated_device", tc.parse_bool, False
    )
    IS_UNAPPROVED_DEVICE = Column(
        "is_unapproved_device", tc.parse_bool, False
    )
    IS_PPSD = Column(
        "is_ppsd", tc.parse_bool, False
    )
    IS_US_EXPORT = Column(
        "is_us_export", tc.parse_bool, False
    )
    BIOSPEC_RETENTION = Column(
        "biospec_retention", str, False
    )
    BIOSPEC_DESCRIPTION = Column(
        "biospec_description", tc.parse_text, False
    )
    IPD_TIME_FRAME = Column(
        "ipd_time_frame", tc.parse_text, False
    )
    IPD_ACCESS_CRITERIA = Column(
        "ipd_access_criteria", tc.parse_text, False
    )
    IPD_URL = Column(
        "ipd_url", str, False
    )
    PLAN_TO_SHARE_IPD = Column(
        "plan_to_share_ipd", str, False
    )
    PLAN_TO_SHARE_IPD_DESCRIPTION = Column(
        "plan_to_share_ipd_description", tc.parse_text, False
    )
    CREATED_AT = Column(
        "created_at", tc.parse_date, False
    )
    UPDATED_AT = Column(
        "updated_at", tc.parse_date, False
    )
