==============
Python scripts
==============

The ctg-aact package contains two scripts, one is used to put together some template files, used to build the ORM classes in the package. The other uses the ORM to build a functional relational database from the AACT clinicaltrials.gov flat files.

.. _ctg_build:

``ctg-aact-build``
------------------

.. argparse::
   :module: ctg.aact.build
   :func: _init_cmd_args
   :prog: ctg-aact-build

Example usage
~~~~~~~~~~~~~

To create the SQLite database called ``ctg-20231117.db`` from the downloaded zip file ``hsrd6rng62raqyc3yqmyahzpao0p.zip``, output with full verbosity.

.. code-block:: console

   $ ctg-aact-build -vv /data/clinical_trials/hsrd6rng62raqyc3yqmyahzpao0p.zip /fast/ctg-20231117.db


Known Issues
~~~~~~~~~~~~

This has only been tested against SQLite and MariaDB. I tested against duck-db and I get an error:

.. code-block:: python

   Traceback (most recent call last):
     File "/miniconda3/lib/python3.8/site-packages/sqlalchemy/engine/base.py", line 1276, in _execute_context
       self.dialect.do_execute(
     File "/miniconda3/lib/python3.8/site-packages/sqlalchemy/engine/default.py", line 608, in do_execute
       cursor.execute(statement, parameters)
     File "/miniconda3/lib/python3.8/site-packages/duckdb_engine/__init__.py", line 142, in execute
       self.__c.execute(statement, parameters)
   duckdb.Error: Dependency Error: Cannot alter entry "studies" because there are entries that depend on it.

   The above exception was the direct cause of the following exception:

   --- REMOVED SOME STACKTRACE ---

   sqlalchemy.exc.DBAPIError: (duckdb.Error) Dependency Error: Cannot alter entry "studies" because there are entries that depend on it.
   [SQL:
   CREATE TABLE brief_summaries (
     id INTEGER NOT NULL,
     nct_id VARCHAR(20) NOT NULL,
     description VARCHAR(5500) NOT NULL,
     PRIMARY KEY (id),
     FOREIGN KEY(nct_id) REFERENCES studies (nct_id)
   )

   ]

I have seen this type of error before with duck-db, I am not sure what causes it everything seems fine with the DB structure and I am not altering anything, just setting up tables. This error is an actual duck-db error as you get the same from a non-SQLAlchemy build.

.. _orm_template:

``ctg-aact-parse-schema``
--------------------------

.. argparse::
   :module: ctg.aact.parse_schema
   :func: _init_cmd_args
   :prog: ctg-aact-parse-schema

Example usage
~~~~~~~~~~~~~

To create the table/column info files with default names and locations, output with full verbosity.

.. code-block:: console

   $ ctg-aact-parse-schema -vv aact_tables.xlsx definitions.csv
   === ctg-aact-parse-schema (ctg_aact v0.1.0a1) ===
   [info] 08:55:20 > column_file value: definitions.csv
   [info] 08:55:20 > out_column_info value: None
   [info] 08:55:20 > out_table_info value: None
   [info] 08:55:20 > table_file value: aact_tables.xlsx
   [info] 08:55:20 > verbose value: 2
   [info] 08:55:21 > table info written to: /data/clinical_trials/ctg_aact_table_info.txt
   [info] 08:55:21 > column info written to: /data/clinical_trials/ctg_aact_column_info.txt
   [info] 08:55:21 > *** END ***

Output files
~~~~~~~~~~~~

By default, this outputs a table-info file called ``ctg_aact_table_info.txt`` and a column-info file called ``ctg_aact_column_info.txt``. These are created in the current working directory. These default names can be changed via the command line. The can be manually modified if needed prior to building the ORM code.

Table info file
...............

.. include:: ./data_dict/table-info.rst

Column info file
................

.. include:: ./data_dict/column-info.rst

Known Issues
~~~~~~~~~~~~

None reported.
