===============
Database schema
===============

Below is a diagram of the currently implemented schema from the ClinicalTrials AACT initiative. The table and column descriptions below and in the ORM API documentation are taken from the `schema documentation <https://aact.ctti-clinicaltrials.org/learn_more>`_.

.. image:: ./_static/ctg-aact-schema.png
   :width: 500
   :alt: AACT clinicaltrails.gov schema
   :align: center

.. include:: ./data_dict/ctg-aact-schema-tables.rst
