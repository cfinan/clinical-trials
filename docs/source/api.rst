===================
``ctg`` package API
===================

This contains a sub-package to build clinicaltrails.gov from the AACT distribution. This is in it's own sub package as I have some build code I will eventually include here as well. I also want to incorporate some analysis code in here.

========================
``ctg.aact`` sub-package
========================

This contains modules for building and interacting with the AACT implementation of the `clincaltrails.gov` database.
..
   ORM API documentation references are dynamically produced at documentation
   build time, this includes a module title
.. include:: ./data_dict/ctg-aact-schema-orm.rst

``ctg.aact.constants`` module
-----------------------------

.. automodule:: ctg.aact.constants

``ctg.aact.build`` module
-------------------------

.. currentmodule:: ctg.aact.build
.. autofunction:: build_clinical_trials

``ctg.aact.parse_schema`` module
--------------------------------

.. currentmodule:: ctg.aact.parse_schema
.. autofunction:: parse_ctg_aact_schema
