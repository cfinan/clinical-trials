.. clinical-trials documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

ClinicalTrails.gov builder
==========================

`clinical-trails <https://gitlab.com/cfinan/clinical-trails>`_ is a package to enable the building of the clinicaltrials.gov database that is distributed by AACT. Note, it is intended for research purposes only and not healthcare implementations. It is also not maintained by the clinical trials initiative.

Currently the core tables are implemented and I am gradually making my way through the refset tables, trying to understand them as I go.

Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Setup

   getting_started

.. toctree::
   :maxdepth: 2
   :caption: Programmer reference

   schema
   scripts
   api

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
